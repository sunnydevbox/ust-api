<?php

use Illuminate\Support\Facades\Storage;
use Sunnydevbox\UST\Models\User;
use Sunnydevbox\UST\Models\Alumni;


Route::get('profile/{file}/{type}', '\Sunnydevbox\UST\Http\Controllers\API\V1\FilesController@getImage')->name('profile.get-image');
Route::get('storage/profile/{file}/{type}', '\Sunnydevbox\UST\Http\Controllers\API\V1\FilesController@getImage')->name('profile.get-image');


Route::get('parse', '\Sunnydevbox\UST\Http\Controllers\API\V1\UsersController@parse');
if (app()->getProvider('\Dingo\Api\Provider\LaravelServiceProvider')) {
    $api = app('Dingo\Api\Routing\Router');
    $configUser = config('tw-user.api.users');

    $api->version('v1', ['middleware' => []], function ($api) {
        $api->post('users/register', '\Sunnydevbox\UST\Http\Controllers\API\V1\UsersController@register')->name('user.register');
        $api->post('users/auth', '\Sunnydevbox\UST\Http\Controllers\API\V1\UsersController@authenticate')->name('user.authenticate');
        $api->post('users/validate', '\Sunnydevbox\UST\Http\Controllers\API\V1\UsersController@validate')->name('user.validate');
        $api->post('user/forgot-password', '\Sunnydevbox\UST\Http\Controllers\API\V1\ForgotPasswordController@sendResetLinkEmail')->name('user.forgot-password');
        $api->post('user/resend-activation', '\Sunnydevbox\UST\Http\Controllers\API\V1\UsersController@resendActivation')->name('user.reset-activation');


        $api->get('aggregates', '\Sunnydevbox\UST\Http\Controllers\API\V1\AggregateController@index');
    });

    $api->version('v1', ['middleware' => ['api.auth']], function ($api) use($configUser) {
        

        // QUICK FIX
        $api->get('user', '\Sunnydevbox\UST\Http\Controllers\API\V1\UsersController@index2')->name('user.index2');
        $api->put('user/{id}', '\Sunnydevbox\UST\Http\Controllers\API\V1\UsersController@update2')->name('user.update2');
        $api->get('user/{id}', '\Sunnydevbox\UST\Http\Controllers\API\V1\UsersController@show2')->name('user.show2');
        $api->post('user/update-email/{id}', '\Sunnydevbox\UST\Http\Controllers\API\V1\UsersController@updateEmail')->name('user.update-email');
        $api->post('user/update-password/{id}', '\Sunnydevbox\UST\Http\Controllers\API\V1\UsersController@updatePassword')->name('user.update-password');

        $api->post('users/logout', '\Sunnydevbox\UST\Http\Controllers\API\V1\UsersController@logout');

        // $api->get('aggregates', '\Sunnydevbox\UST\Http\Controllers\API\V1\AggregateController@index');
        $api->get('profile', '\Sunnydevbox\UST\Http\Controllers\API\V1\UsersController@showProfile');
        $api->put('profile', '\Sunnydevbox\UST\Http\Controllers\API\V1\UsersController@updateProfile');
        $api->put('account', '\Sunnydevbox\UST\Http\Controllers\API\V1\UsersController@updateAccount');
    
        $api->resource('profiles', '\Sunnydevbox\UST\Http\Controllers\API\V1\ProfileController');  

        $api->resource('announcements', '\Sunnydevbox\UST\Http\Controllers\API\V1\AnnouncementsController');
        $api->resource('specialties', '\Sunnydevbox\UST\Http\Controllers\API\V1\SpecialtiesController');

        $api->post('files', '\Sunnydevbox\UST\Http\Controllers\API\V1\FilesController@upload');

        $api->get('clinics/aggregate', '\Sunnydevbox\UST\Http\Controllers\API\V1\ClinicController@aggregate')->name('clinics.aggregate');
        $api->resource('clinics', '\Sunnydevbox\UST\Http\Controllers\API\V1\ClinicController');

        //admin routes
        $api->group(['namespace' => 'Sunnydevbox\UST\Http\Controllers\API\V1\Admin','prefix'=>'admin','middleware' => ['role:admin']], function($api){
            $api->post('announcements/image/{id}', 'AnnouncementsController@attachImage')->name('admin.announcements-attach-image');
            $api->resource('announcements', 'AnnouncementsController');
            $api->post('announcements/{annoucement}/highlight', 'AnnouncementsController@highlight');
            $api->post('announcements/{annoucement}/remove-highlight', 'AnnouncementsController@removeHighlight');
            $api->post('alumni/import', 'UsersController@import');
        });

        $api->group(['namespace' => 'Sunnydevbox\UST\Http\Controllers\API\V1\Admin','prefix'=>'admin','middleware' => []], function($api){
            $api->resource('users', '\Sunnydevbox\UST\Http\Controllers\API\V1\UsersController');
        });

        $api->get('checkout', '\Sunnydevbox\UST\Http\Controllers\API\V1\CheckoutController@getCheckoutDetails');

        $api->post('gallery-images', '\Sunnydevbox\UST\Http\Controllers\API\V1\GalleryPhotoController@attachFile')->name('gallery-photo.attach');
        $api->resource('gallery-images', '\Sunnydevbox\UST\Http\Controllers\API\V1\GalleryPhotoController', ['except' => ['store']]);
        $api->post('galleries/image', '\Sunnydevbox\UST\Http\Controllers\API\V1\GalleryController@attachFile');
        $api->delete('galleries/image', '\Sunnydevbox\UST\Http\Controllers\API\V1\GalleryController@detachFile');
        
        $api->resource('galleries', '\Sunnydevbox\UST\Http\Controllers\API\V1\GalleryController');


        $api->group(['middleware' => ['role:admin']], function($api) use ($configUser) {
            $api->post('users/{id}/assign-roles', $configUser['controller'].'@assignRoles')->name('users.assignroles');
            $api->post('users/{id}/remove-role', $configUser['controller'].'@removeRole')->name('users.removerole');
            $api->get('users/{id}/roles', $configUser['controller'].'@getRoles')->name('users.getroles');
        });
    });





}
