# UST API
UST API. Video conferencing for doctors and patients.


# Installation
1) Install and configure a fresh instance of Laravel 5.4.x
```sh
$ composer create-project laravel/laravel="5.4.*" folder-name
```
2) composer.json: Include this in the repositories section
```javascript
"minimum-stability": "dev",
"repositories": [
    ...
        {
            "type": "git",
            "url": "https://bitbucket.org/sunnydevbox/tw-core.git"
        },
        {
            "type": "git",
            "url": "https://bitbucket.org/sunnydevbox/tw-user.git"
        },
        {
            "type": "git",
            "url": "https://bitbucket.org/sunnydevbox/ust-api.git"
        }
        ...
    ]
```

3) Composer: Require the UST package
```sh
$ composer require sunnydevbox/ust-api:dev-master
```

4) Update config/app.php. Add these under 'providers'
```ssh
Sunnydevbox\UST\USTServiceProvider::class,
```

5) Update
```sh
$ composer dumpautoload
```

# Installation

1) Remove all migration files from database/migrations

2) Configure database settings in .env file
```ssh
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```

3) Publish UST initial migration files
```sh
$ php artisan ust:run-setup 
```

4) Manage UST migrations
```sh
$ php artisan ust:migrate --action=[run/reset/refresh/rollback]
```


5) Configure .env
```javascript
API_NAME="UST API"
API_STRICT=true
API_VERSION=v1
API_PREFIX=api
API_DEFAULT_FORMAT=json
API_DEBUG=true
API_SUBTYPE=ust

//////////////////////////////////
// OPTIONAL FOR development use //
// Note: these values could     //
//       change anytime without //
//       notice                 //
//////////////////////////////////
MAIL_DRIVER=mailgun
MAIL_HOST=smtp.mailgun.org
MAIL_PORT=2525
MAIL_USERNAME=postmaster@domain.com
MAIL_PASSWORD=[PASSWORD]
MAIL_ENCRYPTION=null

MAIL_FROM_ADDRESS=team.domain.com
MAILGUN_DOMAIN=api.domain.com
MAILGUN_SECRET=[EKY]

```

7) 

8) Empty all route files in root directory
```ssh
routes/api.php
routes/channel.php
routes/console.php
routes/web.php
```

9) Update config/api.php
```javascript
'middleware' => [
        \Barryvdh\Cors\HandleCors::class,
    ],
'auth' => [
        'jwt' => 'Dingo\Api\Auth\Provider\JWT',
    ],
```

10) Update config/jwt.php
```javascript
'user' => \Sunnydevbox\UST\Models\User::class,
```
11) Update config/auth.php
```javascript
'providers' => [
        'users' => [
            'driver' => 'eloquent',
            'model' => \Sunnydevbox\UST\Models\User::class,
        ],
],
```

8) Clear all cache
```sh
$ php artisan twcore:optimize
```

# Server Configuration
Coming soon

# Queue Configuration
Coming soon





