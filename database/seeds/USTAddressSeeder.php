<?php

use Illuminate\Database\Seeder;
use Sunnydevbox\UST\Models\Specialty;

class USTAddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $country = new \Sunnydevbox\UST\Models\Country(['name' => 'Philippines', 'country_code' => 'PH']);
        $country->save();
        $province = new \Sunnydevbox\UST\Models\Province(['name' => 'CEBU']);
        $country->provinces()->save($province);

        $cityItems = ['Cebu City', 'Mandaue City', 'Consolacion', 'Liloan', 'Naga', 'Minglanilla', 'Carcar'];

        foreach($cityItems as $cityItem){
            $city = new \Sunnydevbox\UST\Models\City(['name' => $cityItem]);
            $city->province()->associate($province);
            $city->save();
        }
    }
}
