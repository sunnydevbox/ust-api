<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use Sunnydevbox\UST\Models\User;
use Sunnydevbox\UST\Models\Profile;
use Sunnydevbox\UST\Models\Specialty;
use Sunnydevbox\UST\Models\HospitalAffiliation;
use Sunnydevbox\UST\Models\ClinicAddress;
use Sunnydevbox\UST\Services\FileService;
use Sunnydevbox\UST\Services\AggregateService;

class USTUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $fileService = new FileService;
        $aggregates = app(AggregateService::class);
        $aggs = $aggregates->aggregate([
            'cities','provinces','countries'
        ]);

        // $info = pathinfo($url);

        // dd($info);
        

        // $name = substr($url, strrpos($url, '/') + 1);
        // $name = 'profile/';

        // // $url = $faker->imageUrl(1080, 1920, 'people');
        // dd($faker->image(storage_path('app/public'), 200, 200, 'people'));
        // dd($url);
        // dd('https://picsum.photos/id/'.rand(0,1000).'/1080/1920');
        // $contents = file_get_contents('http://picsum.photos/id/'.rand(0,1000).'/1080/680');
        // $name = substr($url, strrpos($url, '/') + 1);
        // Storage::put($name, $contents);

        // file_put_contents(, $image ); 
        // storage_path('app/public/profile/temp.jpg');
        //$i = $fileService->upload();

    	foreach (range(1,100) as $index) {
            $gender = rand(0,1) ? 'male' : 'female';

            // CREATE User
	        $user = User::create([
	            'email' => $faker->email,
                'password' => bcrypt('1234qwer'),
                'first_name' => $faker->firstName($gender),
                'last_name' => $faker->lastName($gender),
                'middle_name' => $faker->lastName($gender),
                'batch' => rand(1960, date('Y')),
                'status'    => 'active',
            ]);
            
            
            // ADD Role
            $user->assignRole('alumni');
            

            $url = 'https://picsum.photos/id/'.rand(0,10).'/1080/1920';
            $contents = null;
            try {
                $contents = file_get_contents($url);
                $filename = 'profile/'.md5(rand(0, 9998) * rand(9999,19998)).'.jpg';
                Storage::put($filename, $contents);
            } catch(\Exception $e) {
                $contents = null;
            }

            // CREA;TE Profile
            $profile = Profile::create([
                'user_id' => $user->id,
                'profile_image' => ($contents) ? $filename : null,
                'street_no' => $faker->buildingNumber(),
                'street' => $faker->streetName(),
                'city' => $aggs['cities'][rand(0,count($aggs['cities'])-1)]['value'],
                'province' => $aggs['provinces'][rand(0,count($aggs['provinces'])-1)]['value'],
                'postal_code' => $faker->areaCode(),
                'country_code'  => $aggs['countries'][rand(0,count($aggs['countries'])-1)]['value'],
                'mobile'    => $faker->tollFreePhoneNumber(),
                'position' => $faker->jobTitle(),
                'society_affiliation' => HospitalAffiliation::inRandomOrder()->first()->id,
                'affiliation_address' => $faker->address(),
                'license_no' => $faker->randomNumber(),
                'specialty_id' => Specialty::inRandomOrder()->first()->id,
                'is_info_shareable' => rand(0,1),
                'is_email_clinical_shareable' => rand(0,1),
            ]);


            foreach(range(0, rand(0,6)) as $i) {
                ClinicAddress::create([
                    'name' => $faker->company(),
                    'user_id' => $user->id,
                    'street_no' => $faker->buildingNumber(),
                    'street' => $faker->streetName(),
                    'city' => $aggs['cities'][rand(0,count($aggs['cities'])-1)]['value'],
                    'province' => $aggs['provinces'][rand(0,count($aggs['provinces'])-1)]['value'],
                    'postal_code' => $faker->areaCode(),
                    'country_code'  => $aggs['countries'][rand(0,count($aggs['countries'])-1)]['value'],
                ]);
            }
        }
    }
}
