<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Sunnydevbox\UST\Models\User;

class USTRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
            ['name' => 'alumni'],
            ['name' => 'admin']
        ];

        foreach($data as $datum){
            $role = Role::firstOrNew([
                'name' => $datum['name']
            ]);
            $role->save();
        }

        $admin = User::firstOrNew([
            'email' => 'admin@ust.com',
        ]);

        $admin->password = '1234qwer';
        $admin->first_name = 'UST';
        $admin->last_name = 'ADMIN';
        $admin->batch = '2012';
        $admin->is_verified = \Carbon\Carbon::now();
        $admin->save();
        $admin->assignRole('admin');
    }
}
