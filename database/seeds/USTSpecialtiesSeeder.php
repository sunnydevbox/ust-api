<?php

use Illuminate\Database\Seeder;
use Sunnydevbox\UST\Models\Specialty;

class USTSpecialtiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $specialties = [
            [
                'name' => 'Anesthesiology',
                'children' => [
                    ['name' => 'Pain Management'],
                    ['name' => 'Pediatric Anesthesiology'],
                    ['name' => 'Ultrasound Guided Anesthesiology'],
                ],
            ],
            ['name' => 'Clinical Nutrition'],
            [
                'name' => 'Dermatology',
                'children' => [
                    ['name' => 'Contact Dermatitis and Environmental Dermatology'],
                    ['name' => 'Dermapathology'],
                    ['name' => 'Dermatologic Surgery'],
                    ['name' => 'Immunodermatology'],
                    ['name' => 'Laser Surgery'],
                    ['name' => 'Leprosy'],
                ],
            ],
            ['name' => 'Emergency Medicine'],
            ['name' => 'Family Medicine'],
            ['name' => 'General Practice'],
            [
                'name' => 'General Surgery',
                'children' => [
                    ['name' => 'Cardiovascular Surgery'],
                    ['name' => 'Colorectal Surgery'],
                    ['name' => 'Head and Neck'],
                    ['name' => 'Hepatobiliary Pancreatic'],
                    ['name' => 'Neurosurgery'],
                    [
                        'name' => 'Orthopedics',
                        'children' => [
                            ['name' => 'Arthroplasty'],
                            ['name' => 'Foot and Ankle'],
                            ['name' => 'Hand and Microvascular'],
                            ['name' => 'Pediatric Orthopedics'],
                            ['name' => 'Trauma'],
                            ['name' => 'Tumor'],
                            ['name' => 'Spine'],
                            ['name' => 'Sports'],
                            ['name' => 'Ultrasound MSK'],
                        ],
                    ],
                    ['name' => 'Pediatric Surgery'],
                    ['name' => 'Plastic and Reconstructive Surgery'],
                    ['name' => 'Surgical Critical Care'],
                    ['name' => 'Surgical Oncology'],
                    ['name' => 'Transplant'],
                    ['name' => 'Trauma'],
                    [
                        'name' => 'Urology',
                        'children' => [
                            ['name' => 'Urologic Oncology'],
                            ['name' => 'Pediatric Urology'],
                            ['name' => 'Endourology & MIU'],
                        ],
                    ],

                ],
            ],
            [
                'name' => 'Internal Medicine',
                'children' => [
                    ['name' => 'Cardiology'],
                    ['name' => 'Endocrinology'],
                    ['name' => 'Gastroenterology'],
                    ['name' => 'Geriatrics'],
                    ['name' => 'Infectious Medicine'],
                    ['name' => 'Immunology'],
                    ['name' => 'Hematology'],
                    ['name' => 'Nephrology'],
                    ['name' => 'Oncology'],
                    ['name' => 'Pulmonology'],
                    ['name' => 'Regenerative Medicine'],
                    ['name' => 'Rheumatology'],
                ],
            ],
            ['name' => 'Medico Legal'],
            [
                'name' => 'Neurology',
                'children' => [
                    ['name' => 'Dementia'],
                    ['name' => 'Epilepsy'],
                    ['name' => 'Headache'],
                    ['name' => 'Interventional Neurology'],
                    ['name' => 'Neurocritical Care'],
                    ['name' => 'Neurophysiology'],
                    ['name' => 'Movement Disorder'],
                    ['name' => 'Neurogenetics'],
                    ['name' => 'Neuroimmunology'],
                    ['name' => 'Neuro-oncology'],
                    ['name' => 'Neuropathology'],
                    ['name' => 'Stroke'],
                ],
            ],
            ['name' => 'Nuclear Medicine'],
            [
                'name' => 'Obstetrics - Gynecology',
                'children' => [
                    ['name' => 'Gynecologic Oncology'],
                    ['name' => 'Infectious Disease'],
                    ['name' => 'Maternal and Fetal Medicine'],
                    ['name' => 'OB-GYN Ultrasound'],
                    ['name' => 'Pediatric and Adolescent Gynecology'],
                    ['name' => 'Reproductive Endocrinology and Infertility'],
                    ['name' => 'Trophoblastic Disease'],
                    ['name' => 'Urogynecology'],
                ],
            ],
            ['name' => 'Occupational Medicine'],
            [
                'name' => 'Ophthalmology',
                'children' => [
                    ['name' => 'Comprehensive Ophthalmology'],
                    ['name' => 'Glaucoma'],
                    ['name' => 'Oculoplastic/Orbit/Lacrimal'],
                ],
            ],
            ['name' => 'Ocular Pathology'],
            ['name' => 'Ocular Genetics'],
            [
                'name' => 'Ocular Oncology',
                'children' => [
                    ['name' => 'Pediatric Ophthalmology/Strabismus'],
                    ['name' => 'Neuro-Ophthalmology'],
                    ['name' => 'Cornea and Refractive Ophthalmology'],
                    ['name' => 'Retina'],
                    ['name' => 'Uveitis'],
                    ['name' => 'External Eye Diseases'],
                    ['name' => 'Low Vision'],
                ],
            ],
            [
                'name' => 'Otolaryngology',
                'children' => [
                    ['name' => 'Laryngology'],
                    ['name' => 'Neurotology'],
                    ['name' => 'Rhinology'],
                    ['name' => 'Skull Base Surgery'],
                ],
            ],
            ['name' => 'Pathology'],
            [
                'name' => 'Pediatrics',
                'children' => [
                    ['name' => 'Adolescent Medicine'],
                    ['name' => 'Allergology/Immunology'],
                    ['name' => 'Ambulatory Pediatrics'],
                    ['name' => 'Critical Care'],
                ],
            ],
            [
                'name' => 'Emergency Medicine',
                'children' => [
                    ['name' => 'Developmental Pediatrics'],
                    ['name' => 'Neonatology'],
                    ['name' => 'Pediatric Cardiology'],
                    ['name' => 'Pediatric Endocrinology and Metabolism'],
                ],
            ],
            [
                'name' => 'Pediatric Genetics',
                'children' => [
                    ['name' => 'Pediatric Hematology and Oncology'],
                    ['name' => 'Pediatric Gastroenterology, Hepatology and Nutrition'],
                    ['name' => 'Pediatric Infectious Disease'],
                    ['name' => 'Pediatric Nephrology'],
                    ['name' => 'Pediatric Neurology'],
                    ['name' => 'Pediatric Pulmonology'],
                    ['name' => 'Pediatric Rheumatology'],
                ],
            ],
            [
                'name' => 'Psychiatry',
                'children' => [
                    ['name' => 'Addiction Psychiatry'],
                    ['name' => 'Child Psychiatry'],
                    ['name' => 'Community Psychiatry'],
                    ['name' => 'Consultation Liason Psychiatry'],
                    ['name' => 'Forensic Psychiatry'],
                ],
            ],
            ['name' => 'Public Health'],
            ['name' => 'Radiology', 'children' => [
                ['name' =>'Breast Imaging'],
                ['name' =>'CT-MRI'],
                ['name' =>'Diagnostic Radiology'],
                ['name' =>'Interventional Radiology'],
                ['name' =>'Neuroradiology'],
                ['name' =>'Pediatric Radiology'],
                ['name' =>'Radiologic Oncology'],
            ]],
            ['name' => 'Rehab Medicine'],
            ['name' => 'Toxicology'],
        ];

        foreach ($specialties as $specialty) {
            $this->addSpecialtyTree($specialty);
        }
    }

    protected function addSpecialtyTree($item, $parent = null)
    {
        $specialty = Specialty::create(['name' => $item['name'], 'parent_id' => $parent ]);

        if(isset($item['children']) && count($item['children']) > 0){
            foreach ($item['children'] as $sub_item) {
                $this->addSpecialtyTree($sub_item, $specialty->id);
            }
        }
    }
}
