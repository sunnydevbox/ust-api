<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Sunnydevbox\UST\Models\User;
use Sunnydevbox\UST\Models\Photo;

class USTCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
            ['name' => 'Education', 'slug' => 'education'],
            ['name' => 'Outreach', 'slug' => 'outreach'],
        ];

        foreach($data as $datum){
            $category = \Sunnydevbox\UST\Models\Category::firstOrNew([
                'slug' => $datum['slug']
            ]);
            $category->name = $datum['name'];

            $category->save();
        }
    }
}
