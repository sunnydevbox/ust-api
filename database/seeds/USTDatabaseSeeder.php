<?php

use Illuminate\Database\Seeder;

class USTDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(USTRolesSeeder::class);
        $this->call(USTSpecialtiesSeeder::class);
        $this->call(USTUserSeeder::class);
    }
}
