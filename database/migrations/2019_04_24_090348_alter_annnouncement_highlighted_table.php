<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAnnnouncementHighlightedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('announcements', function(Blueprint $table){
            $table->timestamp('highlighted_at')->nullable();
        });

        Schema::create('media', function(Blueprint $table){
            $table->increments('id');
            $table->integer('announcement_id')->unsigned();
            $table->string('image');
            $table->timestamps();

            $table->foreign('announcement_id')->references('id')->on('announcements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('announcements', function(Blueprint $table){
            $table->dropColumn('highlighted_at');
        });

        Schema::drop('media');
    }
}
