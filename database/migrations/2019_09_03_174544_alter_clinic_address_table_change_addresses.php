<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterClinicAddressTableChangeAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clinic_addresses', function(Blueprint $table){
            $table->unsignedInteger('city_id')->nullable();
            $table->unsignedInteger('province_id')->nullable();
            $table->unsignedInteger('country_id')->nullable();
            $table->string('city')->nullable()->change();
            $table->string('province')->nullable()->change();
            $table->string('country_code')->nullable()->change();

            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreign('province_id')->references('id')->on('provinces');
            $table->foreign('country_id')->references('id')->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clinic_addresses', function(Blueprint $table){
            $table->dropForeign('clinic_addresses_city_id_foreign');
            $table->dropForeign('clinic_addresses_province_id_foreign');
            $table->dropForeign('clinic_addresses_country_id_foreign');

            $table->dropColumn('city_id');
            $table->dropColumn('province_id');
            $table->dropColumn('country_id');


        });
    }
}
