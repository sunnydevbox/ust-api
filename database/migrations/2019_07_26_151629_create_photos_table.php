<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('photos')) {
            Schema::create('photos', function(Blueprint $table){
                $table->increments('id');
                $table->string('file');
                $table->string('original_filename');
                $table->unsignedInteger('user_id');
                $table->string('batch');
                $table->timestamps();

                $table->foreign('user_id')->references('id')->on('users');
            });
        }

        if (!Schema::hasTable('category_photo')) {
            Schema::create('category_photo', function(Blueprint $table){
                $table->unsignedInteger('category_id');
                $table->unsignedInteger('photo_id');

                $table->foreign('category_id')->references('id')->on('categories');
                $table->foreign('photo_id')->references('id')->on('photos');

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('photos');
        Schema::drop('category_photo');
    }
}
