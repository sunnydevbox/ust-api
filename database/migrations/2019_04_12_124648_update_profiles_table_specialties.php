<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProfilesTableSpecialties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles', function(Blueprint $table){
            $table->dropColumn('specialty');
            $table->dropColumn('sub_specialty');

            $table->integer('specialty_id')->unsigned()->nullable();
            $table->string('specialty_others')->nullable();
            $table->integer('sub_specialty_id')->unsigned()->nullable();
            $table->string('sub_specialty_others')->nullable();
            $table->integer('sub_specialty_id_2')->unsigned()->nullable();
            $table->string('sub_specialty_2_others')->nullable();

            $table->foreign('specialty_id')->references('id')->on('specialties');
            $table->foreign('sub_specialty_id')->references('id')->on('specialties');
            $table->foreign('sub_specialty_id_2')->references('id')->on('specialties');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function(Blueprint $table){
            $table->string('specialty');
            $table->string('sub_specialty');

            $table->dropColumn('specialty_id');
            $table->dropColumn('specialty_others');
            $table->dropColumn('sub_specialty_id');
            $table->dropColumn('sub_specialty_others');
            $table->dropColumn('sub_specialty_id_2');
            $table->dropColumn('sub_specialty_2_others');
        });
    }
}
