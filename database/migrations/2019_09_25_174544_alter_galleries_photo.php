<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGalleriesPhoto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gallery_photos', function(Blueprint $table){
            $table->integer('batch')->nullable();
            $table->integer('category_id')->unsigned()->nullable();
            
            $table->index('category_id');
            $table->foreign('category_id')
                ->references('id')
                ->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gallery_photos', function($table) {
            $table->dropForeign('gallery_photos_category_id_foreign');
            $table->dropIndex('gallery_photos_category_id_index');
            $table->dropColumn(['category_id']);
        });
    }
}
