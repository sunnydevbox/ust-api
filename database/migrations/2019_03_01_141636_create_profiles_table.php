<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function(Blueprint $table){
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('profile_image')->nullable();
            $table->string('street_no', 50)->nullable();
            $table->string('street', 100);
            $table->string('city', '30');
            $table->string('province', '30');
            $table->string('postal_code', 10);
            $table->string('country_code', 4);
            $table->string('mobile');
            $table->string('specialty')->nullable();
            $table->string('sub_specialty')->nullable();
            $table->string('position')->nullable();
            $table->smallInteger('society_affiliation');
            $table->string('affiliation_address');
            $table->string('license_no', 100);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('hospital_affiliations', function(Blueprint $table){
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('clinic_addresses', function(Blueprint $table){
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name', 100);
            $table->string('street_no', 50)->nullable();
            $table->string('street', 100);
            $table->string('city', '30');
            $table->string('province', '30');
            $table->string('postal_code', 10);
            $table->string('country_code', 4);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profiles');
        Schema::drop('hospital_affiliations');
        Schema::drop('clinic_addresses');
    }
}
