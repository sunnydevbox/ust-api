<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGalleries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('galleries')) {
            Schema::create('galleries', function(Blueprint $table){
                $table->increments('id')->unsigned();
                $table->integer('author_id')->unsigned();
                $table->string('name');
                $table->text('description')->nullable();
                $table->string('category')->nullable();
                $table->integer('batch')->nullable();
                $table->integer('count')->default(0)->nullable();
                $table->string('status')->defalult('pending');
                $table->timestamps();

                $table->index('author_id');
                $table->foreign('author_id')
                    ->references('id')
                    ->on('users');
            });
        }

        if (!Schema::hasTable('gallery_photos')) {
            Schema::create('gallery_photos', function ($table) {
                $table->increments('id')->unsigned();
                $table->integer('gallery_id')->unsigned();
                $table->integer('author_id')->unsigned();
                $table->string('title'); 
                $table->text('description')->nullable();
                $table->string('filename');
                $table->string('status')->defalult('pending');
                $table->timestamps();

                $table->index('gallery_id');
                $table->foreign('gallery_id')
                    ->references('id')
                    ->on('galleries');

                $table->index('author_id');
                $table->foreign('author_id')
                    ->references('id')
                    ->on('users');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gallery_photos', function($table) {
            $table->dropForeign('gallery_photos_gallery_id_foreign');
            $table->dropForeign('gallery_photos_author_id_foreign');

            $table->dropIndex('gallery_photos_gallery_id_index');
            $table->dropIndex('gallery_photos_author_id_index');
        });
        
        Schema::table('galleries', function($table) {
            $table->dropForeign('galleries_author_id_foreign');
            $table->dropIndex('galleries_author_id_index');
        });
        

        Schema::dropIfExists('gallery_photos');
        Schema::dropIfExists('galleries');
    }
}
