<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcements', function(Blueprint $table){
            $table->increments('id');
            $table->string('title');
            $table->text('body');
            $table->boolean('is_display_donate');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('announcement_user', function(Blueprint $table){
            $table->integer('user_id')->unsigned();
            $table->integer('announcement_id')->unsigned();
            $table->timestamp('viewed_at')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('announcement_id')->references('id')->on('announcements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('announcement_user');
        Schema::drop('announcements');
    }
}
