<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('messages')) {
            Schema::create('messages', function(Blueprint $table){
                $table->increments('id');
                $table->uuid('conversation_id');
                $table->unsignedInteger('sender_id');
                $table->unsignedInteger('receiver_id');
                $table->text('content');
                $table->timestamp('read_at')->nullable();
                $table->timestamps();

                $table->foreign('sender_id')->references('id')->on('users');
                $table->foreign('receiver_id')->references('id')->on('users');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('messages');
    }
}
