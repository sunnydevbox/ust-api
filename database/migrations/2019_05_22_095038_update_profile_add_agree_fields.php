<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProfileAddAgreeFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles', function(Blueprint $table){
            $table->boolean('is_info_shareable')->default(false);
            $table->boolean('is_email_clinical_shareable')->default(false);
            $table->dropColumn('visibility');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function(Blueprint $table){
            $table->dropColumn('is_info_shareable');
            $table->dropColumn('is_email_clinical_shareable');
            $table->integer('visibility');
        });
    }
}
