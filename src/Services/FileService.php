<?php
namespace Sunnydevbox\UST\Services;

class FileService
{
    public function upload($data)
    {
        $target = $data['target'];
        $file = $data['file']->store($target);

        return [
            'path' => $file, 
            'url' => \Storage::url($file),
        ];
    }
}
