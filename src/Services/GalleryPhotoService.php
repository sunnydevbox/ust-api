<?php
namespace Sunnydevbox\UST\Services;

use Sunnydevbox\UST\Repositories\Gallery\GalleryPhotoRepository;
use Sunnydevbox\UST\Validators\GalleryPhotoValidator;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use \Prettus\Validator\Contracts\ValidatorInterface;
use Image;
use File;
use Storage;

class GalleryPhotoService
{
    public function attachFile($data)
    {
        try {
            $data['author_id'] = \Auth::user()->id;
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
            
            $photo = $this->repository->create(
                collect($data)
                ->only($this->repository->makeModel()->getFillable())
                ->all()
            );
            $m = $this->repository->makeModel();

            if ($photo && $data['image']) {
                $this->generate($photo, $data['image']);
                
                return $photo->attachments;
            }
            
        } catch (ModelNotFoundException $e) {
            throw new ModelNotFoundException($e);            
        } catch (ValidatorException $e) {
            throw new ValidatorException($e->getMessageBag());            
        } catch (\Exception $e) {
            if (method_exists($e, 'getMessageBag')) {
                $message = $e->getMessageBag();
            } else if (method_exists($e, 'getMessage')) {
                $message = $e->getMessage();
            } else {
                $message = 'None';
            }
            throw new \Exception($message, $e->getCode());            
        }
    }

    public function generate($photo, $image)
    {
        // if ($photo instanceof $this->repository->makeModel() )
        $galleryId = $photo->gallery_id;
        $path = "gallery/";

        if(!Storage::disk('public')->has(storage_path("app/public/{$path}"))){
            File::makeDirectory(storage_path("app/public/{$path}"),0775, true, true);
        }

        $filename = "{$galleryId}_" . date('mdYHis') . uniqid() . ".{$image->getClientOriginalExtension()}";
        $photo->filename = $filename;
        $photo->save();

        $img = Image::make($image);
        $origImage = storage_path("app/public/{$path}{$filename}");
        
        // *** TRANSFER to events
        // STORE ORIG LARGE FILE
        $img->save($origImage);
        $file = $photo->attach($origImage, [
            'group' => "photo_orig",
        ]);

        // STORE FULL LARGE FILE
        $img = Image::make($origImage);
        $mobileFullFile = storage_path("app/public/{$path}m_full_{$filename}");
        $img->resize(1080, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $img->save($mobileFullFile);
        $file = $photo->attach($mobileFullFile, [
            'group' => "photo_full",
        ]);

        // GENERATE THUMBNAIL for mobile
        $img = Image::make($origImage);
        $mobileTHFile = storage_path("app/public/{$path}m_th_{$filename}");
        $img->fit(200); 
        $img->save($mobileTHFile);
        $file = $photo->attach($mobileTHFile, [
            'group' => "mobile_th",
        ]);

        // GENERATE PREVIEW for mobile
        $img = Image::make($origImage);
        $mobilePVFile = storage_path("app/public/{$path}m_pv_{$filename}");
        $img->resize(700, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $img->save($mobilePVFile);
        $file = $photo->attach($mobilePVFile, [
            'group' => "mobile_pv",
        ]);

        unlink($origImage);
        unlink($mobileFullFile);
        unlink($mobileTHFile);
        unlink($mobilePVFile);
    }

    public function detachPhoto($photoId)
    {
        try {
            $photo = $this->repository->find($photoId);
            
            if ($photo) {
                $photo->attachments->each(function ($item, $key) {
                    $item->delete();
                });

                return $photo;
            }
    
        } catch (ModelNotFoundException $e) {
            throw new ModelNotFoundException($e);            
        } catch (ValidatorException $e) {
            throw new ValidatorException($e->getMessageBag());            
        } catch (\Exception $e) {
            if (method_exists($e, 'getMessageBag')) {
                $message = $e->getMessageBag();
            } else if (method_exists($e, 'getMessage')) {
                $message = $e->getMessage();
            } else {
                $message = 'None';
            }
            throw new \Exception($message, $e->getCode());            
        }
    }

    public function updateGalleryCount($gallery)
    {
        $gallery->count = $gallery->attachments()->count();
        $gallery->save();
    }

    public function __construct(
        GalleryPhotoRepository $repository,
        GalleryPhotoValidator $validator
    ) {
        $this->repository = $repository;
        $this->validator = $validator;
    }
}
