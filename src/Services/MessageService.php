<?php

namespace Sunnydevbox\UST\Services;

use Illuminate\Container\Container as Application;
use Ramsey\Uuid\Uuid;
use Sunnydevbox\UST\Notifications\AnnouncementNotification;
use Sunnydevbox\UST\Notifications\MessageNotification;
use Sunnydevbox\UST\Repositories\Message\MessageRepository;
use Sunnydevbox\UST\Models\User;
use Sunnydevbox\UST\Repositories\User\UserRepository;

final class MessageService
{
    private $messageRepository;

    public function __construct(MessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }

    public function send(User $sender, User $receiver, array $data)
    {
        $conversationId = $this->messageRepository->makeModel()->getConversationId($sender, $receiver) ?: Uuid::uuid4();

        $message = $this->messageRepository->makeModel();
        $message->fill([
            'conversation_id' => $conversationId,
            'sender_id' => $sender->id,
            'receiver_id' => $receiver->id,
            'content' => $data['content']
        ]);
        $message->sender()->associate($sender);
        $message->receiver()->associate($receiver);

        $receiver->notify(new MessageNotification($message));

        return $message->save();
    }
}
