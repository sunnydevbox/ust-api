<?php
namespace Sunnydevbox\UST\Services;

use Sunnydevbox\UST\Repositories\Gallery\GalleryRepository;
use Sunnydevbox\UST\Validators\GalleryValidator;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Sunnydevbox\UST\Services\GalleryPhotoService;

class GalleryService
{
    public function attachFile($data)
    {
        try {
            $data['author_id'] = \Auth::user()->id;

            $this->validator->with($data)->passesOrFail('ATTACH_IMAGE');
            
            $gallery = $this->repository->find($data['gallery_id']);
            
            if ($gallery) {

                $photo = $this->photoService->attachFile($data);

                // UPDATE Gallery iamge count
                $this->updateGalleryCount($gallery);

                return $photo;
            }
        } catch (ModelNotFoundException $e) {
            throw new ModelNotFoundException($e);            
        } catch (ValidatorException $e) {
            throw new ValidatorException($e->getMessageBag());            
        } catch (\Exception $e) {
            if (method_exists($e, 'getMessageBag')) {
                $message = $e->getMessageBag();
            } else if (method_exists($e, 'getMessage')) {
                $message = $e->getMessage();
            } else {
                $message = 'None';
            }
            throw new \Exception($message, $e->getCode());            
        }
    }

    public function detachFile($data)
    {
        try {
			$this->validator->with($data)->passesOrFail('DETACH_IMAGE');
            
            $photo = $this->photoService->detachPhoto($data['photo_id']);

            return $photo;
        } catch (ModelNotFoundException $e) {
            throw new ModelNotFoundException($e);            
        } catch (ValidatorException $e) {
            throw new ValidatorException($e->getMessageBag());            
        } catch (\Exception $e) {
            if (method_exists($e, 'getMessageBag')) {
                $message = $e->getMessageBag();
            } else if (method_exists($e, 'getMessage')) {
                $message = $e->getMessage();
            } else {
                $message = 'None';
            }
            throw new \Exception($message, $e->getCode());            
        }
    }

    public function updateGalleryCount($gallery)
    {
        $gallery->count = $gallery->photos->count();
        $gallery->save();
    }

    public function __construct(
        GalleryRepository $repository,
        GalleryValidator $validator,
        GalleryPhotoService $photoService
    ) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->photoService = $photoService;
    }
}
