<?php
namespace Sunnydevbox\UST\Services;

use Sunnydevbox\UST\Repositories\Profile\ProfileRepository;
use Sunnydevbox\UST\Repositories\Specialties\SpecialtiesRepository;
use Sunnydevbox\UST\Repositories\User\UserRepository;
use Sunnydevbox\UST\Repositories\User\AlumnusRepository;
use Sunnydevbox\UST\Repositories\Clinic\ClinicRepository;
use Sunnydevbox\UST\Repositories\Category\CategoryRepository;
use Sunnydevbox\UST\Repositories\Affiliation\HospitalAffiliationRepository;
use Countries;

class AggregateService
{
    public function aggregate($params)
    {
        $params = collect($params);
        $data = [];
        
        if ($params->containsStrict('countries')) {

            //$countriesObj = app('pragmarx.countries')->all();
            $data['countries'] = [ 
                [
                    'value' => 'PH',
                    'name' => 'Philippines'
                ],
                [
                    'value' => 'OTHER',
                    'name' => 'Other'
                ]
                
            ];
            // foreach($countriesObj as $country) {
            //     $data['countries'][$country['cca2']] = $country['name']['common'];
            // }
        }

        if ($params->containsStrict('cities')) {
            
            $data['cities'] = app('pragmarx.countries')
                    ->where('cca2', 'PH')
                    ->first()
                    ->states
                    ->sortBy('name')
                    ->map(function($item) {
                        return [
                            'value' => $item['name'],
                            'name' => $item['name']
                        ];
                    })
                    ->values()
                    ;
        }

        if ($params->containsStrict('provinces')) {
            $data['provinces'] = app('pragmarx.countries')
            ->where('cca2', 'PH')
            ->first()
            ->states
            ->sortBy('name')
            ->map(function($item) {
                return [
                    'value' => $item['name'],
                    'name' => $item['name']
                ];
            })
            ->values()
            ;
        }

        if ($params->containsStrict('specialties')) {
            $data['specialties'] = $this->rpoSpecialty->makeModel()
                                ->select(['id', 'name', 'parent_id'])
                                ->whereNotNull('name')
                                //->whereNull('parent_id') // GET FIRST LAYER ONLY
                                ->orderBy('name', 'asc')
                                ->get()
                                ->map(function($item) { 
                                    return [
                                        'value' => $item['id'],
                                        'name' => ucwords($item['name']),
                                        'parent_id' => $item['parent_id']
                                    ];
                                });
        }

        if ($params->containsStrict('batches')) {
            $data['batches'] = collect(range(date("Y"), 1950))->map(function($item) { 
                return [
                    'value' => $item,
                    'name' => ucwords($item)
                ];
            });            
        }

        if ($params->containsStrict('batches_db')) {
            $data['batches_db'] = $this->rpoAlumnusRepository
                ->makeModel()
                ->select('batch')
                ->groupBy('batch')
                ->orderBy('batch', 'desc')
                ->get()
                ->map(function($item) { 
                    return [
                        'value' => $item['batch'],
                        'name' => ucwords($item['batch'])
                    ];
                });         
        }

        if ($params->containsStrict('hospital-affiliations')) {
            $data['hospital-affiliations'] = $this->rpoHospitalAffiliation->makeModel()
                                ->select(['id', 'name'])
                                ->orderBy('name', 'asc')
                                ->get();
        }

        if ($params->containsStrict('categories')) {
            $data['categories'] = $this->rpoCategory->makeModel()
                                ->select(['id', 'name', 'slug'])
                                ->orderBy('name', 'asc')
                                ->get();
        }

        if ($params->containsStrict('society-affiliations')) {
            $data['society-affiliations'] = [
                [
                    'value' => 2,
                    'name' => 'Regular',
                ],
                [
                    'value' =>  3,
                    'name' => 'Diplomate',
                ],
                [
                    'value' =>  4,
                    'name' => 'Fellow',
                ],
                [
                    'value' =>  5,
                    'name' => 'Life',
                ],
            ];
        }

        return $data;
    }

    public function __construct(
        ProfileRepository $rpoProfile,
        SpecialtiesRepository $rpoSpecialty,
        UserRepository $rpoUser,
        ClinicRepository $rpoClinic,
        HospitalAffiliationRepository $rpoHospitalAffiliation,
        CategoryRepository $rpoCategory,
        AlumnusRepository $rpoAlumnusRepository
    ) {
        $this->rpoProfile = $rpoProfile;
        $this->rpoSpecialty = $rpoSpecialty;
        $this->rpoUser = $rpoUser;
        $this->rpoClinic = $rpoClinic;
        $this->rpoHospitalAffiliation = $rpoHospitalAffiliation;
        $this->rpoCategory = $rpoCategory;
        $this->rpoAlumnusRepository = $rpoAlumnusRepository;
    }
}