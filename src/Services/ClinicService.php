<?php
namespace Sunnydevbox\UST\Services;

use Sunnydevbox\UST\Repositories\Clinic\ClinicRepository;

class ClinicService
{
    public function aggregate()
    {
        $countries = $this->repository->makeModel()
                        ->select('country_code')
                        ->where('country_code', '!=', '')
                        ->orderBy('country_code', 'asc')
                        ->groupBy('country_code')
                        ->get()
                        ->map(function($item) { return [
                            'value' => ucwords($item['country_code']),
                            'name' => ucwords($item['country_code'])
                        ]; });
        $cities = $this->repository->makeModel()
                    ->select('city')
                    ->where('city', '!=', '')
                    ->orderBy('city', 'asc')
                    ->groupBy('city')
                    ->get()
                    ->map(function($item) { return [
                        'value' => ucwords($item['city']),
                        'name' => ucwords($item['city'])
                    ]; });
                    //->pluck(['city'])
                    ;
        // dd($cities);
        $provinces = $this->repository->makeModel()
                        ->select('province')
                        ->where('province', '!=', '')
                        ->orderBy('province', 'asc')
                        ->groupBy('province')
                        ->get()
                        ->map(function($item) { return [
                            'value' => ucwords($item['province']),
                            'name' => ucwords($item['province'])
                        ]; });


        return [
            'countries' => $countries,
            'cities' => $cities,
            'provinces' => $provinces,
        ];
    }

    public function __construct(ClinicRepository $repository)
    {
        $this->repository = $repository;
    }
}