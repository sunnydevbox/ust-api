<?php
namespace Sunnydevbox\UST\Services;

use Sunnydevbox\UST\Repositories\Profile\ProfileRepository;
use Sunnydevbox\UST\Repositories\User\UserRepository;
use Sunnydevbox\UST\Repositories\Clinic\ClinicRepository;
use Prettus\Validator\Exceptions\ValidatorException;
use Sunnydevbox\UST\Validators\UserValidator;
use Sunnydevbox\UST\Repositories\Affiliation\HospitalAffiliationRepository;
use Hash;
use Auth;

class UserService
{

    public function updateAlumni($data, $id)
    {
        try {
            $userData = collect($data['user'])->only($this->rpoUser->makeModel()->getFillable())->all();
            $profileData = collect($data['profile'])->only($this->rpoProfile->makeModel()->getFillable())->all();

            $user = $this->rpoUser->update($userData, $id);
            
            // *** PROFILE
            // REMOVE profile_image because we are using
            // an uploader for this
            // unset($profileData['profile_image']);
            $profile = $this->rpoProfile->findWhere(['user_id' => $user->id])->first();
            
            if ($profile) {
                $profile->fill($profileData)->save();
            } else {
                $profileData['user_id'] = $user->id;
                $this->rpoProfile->create($profileData);
            }

            // grab the hospital field
            
            if (isset($data['hospital_affiliations'])) {
                foreach($data['hospital_affiliations'] as $hospital) {
                    $this->rpoHospitalAffilation->attachToUser($hospital['id'], $user->id, $hospital['name']);
                }
            }

            $user->touch();
            
            // dd($profile);
            // $user->profile()->update($profileData);

            // *** CLINCIS
            // $clinicData = $data['clinics'];
            // foreach($clinicData as $clinic) {
            //     $clinic['user_id'] = $user->id;

            //     // Determines if "id" is present
            //     // this is used for the updateOrCreate condition
            //     $toCheck = [];
            //     if (isset($clinic['id'])) {
            //         $toCheck = [
            //             'id' => $clinic['id']
            //         ];
            //     }
            //     $clinic = collect($clinic)->only($this->rpoClinic->makeModel()->getFillable())->all();
                
            //     $this->rpoClinic->updateOrCreate($toCheck, $clinic);
            // }

            return $user;
        } catch(ValidatorException $e) {
           // dd(1, $e, $e->getMessageBag());
            throw new ValidatorException($e->getMessageBag());
        } catch(\Exception $e) {
            throw new \Exception($e->getMessage());
        }

    }

    public function resendActivation($email)
    {
        // dd($email);
        
        if ($user = $this->rpoUser->findWhere(['email' => $email])->first()) {

            // REgenerate verification token
            $user->verification_token = $this->rpoUser->setVerificationToken();
            $user->save();
        
            event(new \Sunnydevbox\UST\Events\ResendUserActivationCode($user));

            return true;
        } else {
            throw new \Exception('Invalid email', 400);
        }
    }

    public function updatePassword($id, $data)
    {
        try {
            $this->validator->with($data)->passesOrFail('UPDATE_PASSWORD');

            $user = $this->rpoUser->find($id);

            // dd(Hash::make('1234qwer'));

            // VALIDATE CURRENT PASSWORD
            if (!Hash::check($data['password'], $user->password)) {
                throw new \Exception('Invalid Password', 400);
            }
            

            // PROCEED WITH THE UPDATING
            $user->password = $data['new_password'];
            $user->save();

            // EVENT should be triggered here

            return true;

        } catch (ValidatorException $e) {
            throw new ValidatorException($e->getMessageBag());            
        } catch (\Exception $e) {
            if (method_exists($e, 'getMessageBag')) {
                $message = $e->getMessageBag();
            } else if (method_exists($e, 'getMessage')) {
                $message = $e->getMessage();
            } else {
                $message = 'None';
            }
            throw new \Exception($message, $e->getCode());            
        }
    }

    public function updateEmail($id, $data)
    {
        try {
            $this->validator->with($data)->passesOrFail('UPDATE_EMAIL');

            $user = $this->rpoUser->find($id);

            // VALIDATE CURRENT PASSWORD
            if (!Hash::check($data['password'], $user->password)) {
                throw new \Exception('Invalid Password', 400);
            }
            

            // PROCEED WITH THE UPDATING
            $user->email = $data['email'];
            $user->save();

            // EVENT should be triggered here

            return true;

        } catch (ValidatorException $e) {
            throw new ValidatorException($e->getMessageBag());            
        } catch (\Exception $e) {
            if (method_exists($e, 'getMessageBag')) {
                $message = $e->getMessageBag();
            } else if (method_exists($e, 'getMessage')) {
                $message = $e->getMessage();
            } else {
                $message = 'None';
            }
            throw new \Exception($message, $e->getCode());            
        }
    }

    public function __construct(
        UserRepository $rpoUser,
        ProfileRepository $rpoProfile,
        ClinicRepository $rpoClinic,
        UserValidator $validator,
        HospitalAffiliationRepository $rpoHospitalAffilation
    ) {
        $this->rpoUser = $rpoUser;
        $this->rpoProfile = $rpoProfile;
        $this->rpoClinic = $rpoClinic;
        $this->validator = $validator;
        $this->rpoHospitalAffilation = $rpoHospitalAffilation;
    }
}