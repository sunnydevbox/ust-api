<?php
namespace Sunnydevbox\UST\Services;

class PayService
{
    protected $url;

    public function __construct()
    {
        $this->url = env('PAY_URL', 'https://test.oppwa.com/v1/checkouts');
    }

    private function doPostRequest(string $data) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization:Bearer '.env('PAY_AUTHORIZATION', 'OGE4Mjk0MTc0YjdlY2IyODAxNGI5Njk5MjIwMDE1Y2N8c3k2S0pzVDg=')));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        if(curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);
        return $responseData;
    }

    public function doPrepareCheckout($data)
    {
        return $this->doPostRequest($data);
    }
}
