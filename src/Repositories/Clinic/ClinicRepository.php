<?php

namespace Sunnydevbox\UST\Repositories\Clinic;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Sunnydevbox\UST\Models\Specialty;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;

class ClinicRepository extends TWBaseRepository implements CacheableInterface
{
    use CacheableRepository;

    protected $cacheMinutes = 90;

    

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'Sunnydevbox\UST\Models\ClinicAddress';
    }

    public function validator()
    {
        return \Sunnydevbox\UST\Validators\ClinicAddressValidator::class;
    }
}