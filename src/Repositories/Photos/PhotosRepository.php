<?php

namespace Sunnydevbox\UST\Repositories\Photos;

use Prettus\Repository\Events\RepositoryEntityDeleted;
use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Sunnydevbox\UST\Models\Photo;
use Sunnydevbox\UST\Models\Specialty;
use Sunnydevbox\UST\Models\User;

class PhotosRepository extends TWBaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'Sunnydevbox\UST\Models\Photo';
    }

    public function addPhoto(User $user, array $data)
    {
        $photo = new Photo([
            'file' => $data['file'],
            'original_filename' => $data['original_filename'],
            'user_id' => $user->id,
            'batch' => $data['batch']
        ]);

        $photo->save();
        $photo->categories()->sync($data['category_ids']);

        return $photo;
    }

    public function updatePhoto(Photo $photo, array $data)
    {
        $photo->batch = $data['batch'];
        $photo->categories()->sync($data['category_ids']);
        $photo->save();

        return $photo;
    }

    public function delete($id)
    {
        $this->applyScope();

        $temporarySkipPresenter = $this->skipPresenter;
        $this->skipPresenter(true);

        $model = $this->find($id);
        $originalModel = clone $model;

        $this->skipPresenter($temporarySkipPresenter);
        $this->resetModel();

        $model->categories()->detach();
        $deleted = $model->delete();

        event(new RepositoryEntityDeleted($this, $originalModel));

        return $deleted;
    }
}