<?php
namespace Sunnydevbox\UST\Repositories\Category;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;


class CategoryRepository extends TWBaseRepository
{
    protected $fieldSearchable = [
        'id',
        'name',
        'slug',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return \Sunnydevbox\UST\Models\Category::class;
    }
}