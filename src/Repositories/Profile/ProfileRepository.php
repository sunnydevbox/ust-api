<?php

namespace Sunnydevbox\UST\Repositories\Profile;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Sunnydevbox\UST\Models\Specialty;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;

class ProfileRepository extends TWBaseRepository implements CacheableInterface
{
    use CacheableRepository;

    protected $cacheMinutes = 90;

    public function validator()
    {
        return \Sunnydevbox\UST\Validators\ProfileValidator::class;
    }
    

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'Sunnydevbox\UST\Models\Profile';
    }
}