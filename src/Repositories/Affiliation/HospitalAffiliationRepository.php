<?php
namespace Sunnydevbox\UST\Repositories\Affiliation;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;


class HospitalAffiliationRepository extends TWBaseRepository
{
    protected $fieldSearchable = [
        'id',
        'name'
    ];
    
    public function attachToUser($id, $userId, $name)
    {
        try {
            if ($id) {
                $h = $this->find($id);
                if ($h) {
                    if (!$name || !strlen($name)) {
                        $h->delete();
                    } else {
                        $h->name = $name;
                        $h->update();
                        return $h;
                    }
                }
            } else {
                return $this->makeModel()->create([
                    'user_id' => $userId,
                    'name' => $name
                ]);
            }
        } catch(\Exception $e) {
            
        }
    }

    public function countUserAffiliation($userId)
    {
        return $this->makeModel()->where('user_id', $userId)->count();
    }


    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'Sunnydevbox\UST\Models\HospitalAffiliation';
    }
}