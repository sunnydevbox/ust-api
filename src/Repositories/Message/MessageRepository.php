<?php

namespace Sunnydevbox\UST\Repositories\Message;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Sunnydevbox\UST\Models\Message;
use Sunnydevbox\UST\Models\Specialty;
use Sunnydevbox\UST\Models\User;

class MessageRepository extends TWBaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'Sunnydevbox\UST\Models\Message';
    }

    public function push(Message $message)
    {
        return $message->push();
    }

    public function getMessages(User $sender, User $receiver)
    {
        $conversation_id = $this->makeModel()->getConversationId($sender, $receiver);

        if (! $conversation_id){
            return [];
        }

        return $this->makeModel()->where('conversation_id', $conversation_id)
            ->orderBy('created_at', 'DESC')->paginate();
    }

}