<?php

namespace Sunnydevbox\UST\Repositories\Specialties;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Sunnydevbox\UST\Models\Specialty;

class SpecialtiesRepository extends TWBaseRepository
{
    protected $fieldSearchable = [
        'id',
        'parent_id',
        'name',
    ];
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'Sunnydevbox\UST\Models\Specialty';
    }

    public function getAllSpecialties()
    {
        return $this->model->whereNull('parent_id')->get();
    }
}