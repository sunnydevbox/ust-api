<?php

namespace Sunnydevbox\UST\Repositories\Address;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Sunnydevbox\UST\Models\Announcement;
use Sunnydevbox\UST\Models\Medium;
use Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Spatie\Permission\Models\Role;
use Auth;
use Carbon\Carbon;

class ProvinceRepository extends TWBaseRepository
{

    protected $fieldSearchable = [
        'id',
        'name'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'Sunnydevbox\UST\Models\Province';
    }

    public function allByCountry($country_id)
    {
        return $this->makeModel()->where('country_id', $country_id)->get();
    }
}