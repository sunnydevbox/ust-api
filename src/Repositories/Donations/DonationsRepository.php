<?php

namespace Sunnydevbox\UST\Repositories\Specialties;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Sunnydevbox\UST\Models\Specialty;

class DonationsRepository extends TWBaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'Sunnydevbox\UST\Models\Donation';
    }
}