<?php

namespace Sunnydevbox\UST\Repositories\Gallery;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class GalleryRepository extends TWBaseRepository
{
    protected $fieldSearchable = [
        'id',
        'author_id',
        'status',
        'batch',
        'count',
        'name' => 'like',
    ];
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return \Sunnydevbox\UST\Models\Gallery::class;
    }

    public function validator()
    {
        return \Sunnydevbox\UST\Validators\GalleryValidator::class;
    }

}