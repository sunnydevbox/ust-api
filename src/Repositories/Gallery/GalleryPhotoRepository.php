<?php

namespace Sunnydevbox\UST\Repositories\Gallery;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class GalleryPhotoRepository extends TWBaseRepository
{
    protected $fieldSearchable = [
        'id',
        'gallery_id',
        'author_id',
        'status',
        'category_id',
        'batch',
    ];
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return \Sunnydevbox\UST\Models\GalleryPhoto::class;
    }

    public function validator()
    {
        return \Sunnydevbox\UST\Validators\GalleryPhotoValidator::class;
    }

}