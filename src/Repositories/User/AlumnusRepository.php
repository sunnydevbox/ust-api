<?php

namespace Sunnydevbox\UST\Repositories\User;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Spatie\Permission\Models\Role;
use Auth;
use Carbon\Carbon;

class AlumnusRepository extends TWBaseRepository
{

    protected $fieldSearchable = [
        'id',
        'first_name',
        'last_name',
        'batch'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'Sunnydevbox\UST\Models\Alumnus';
    }

    public function saveOrUpdateAlumni(object $data)
    {
         $alumnus =  $this->model->firstOrNew([
                 'first_name' => $data->first_name,
                 'last_name' => $data->last_name,
                 'batch' => substr($data->batch, 0, 4)
             ]
         );

        $alumnus->save();
    }
}