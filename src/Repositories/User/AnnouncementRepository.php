<?php

namespace Sunnydevbox\UST\Repositories\User;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Sunnydevbox\UST\Models\Announcement;
use Sunnydevbox\UST\Models\Medium;
use Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Spatie\Permission\Models\Role;
use Auth;
use Carbon\Carbon;

class AnnouncementRepository extends TWBaseRepository
{

    protected $fieldSearchable = [
        'id',
        'title' => 'like',
        'body' => 'like',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'Sunnydevbox\UST\Models\Announcement';
    }

    public function getAllAnnouncements()
    {
        return $this->makeModel()->all();
    }

    public function viewAnnouncement($id)
    {
        $announcement = $this->makeModel()->findOrFail($id);
        $this->markAnnouncementAsRead($announcement);

        return $announcement;
    }

    protected function markAnnouncementAsRead($announcement)
    {
        if (!$announcement->users()->where('user_id', Auth::user()->id)->whereNotNull('viewed_at')->count()) {
            $announcement->users()->attach(Auth::user()->id, ['viewed_at' => Carbon::now()]);
        }

        return true;
    }

    public function saveAnnouncement($request, $id = null)
    {
        $announcement = $this->model->firstOrNew(['id' => $id]);

        $announcement->fill($request->except(['highlighted_at']));

        $media_items = $request->get('media', []);


        $media = collect();

        foreach ($media_items as $item) {

            $id = isset($item['id']) ? $item['id'] : null;

            $medium = Medium::firstOrNew(['id' => $id]);
            $medium->fill($item);
            $media->push($medium);
        }

        $announcement->setRelation('media', $media);

        $announcement->push();

        return $announcement;
    }

    public function highlight($id)
    {
        $announcement = $this->model->findOrFail($id);

        $this->setAllUnhighlighted();

        $announcement->highlighted_at = Carbon::now();
        $announcement->save();
    }

    public function removeHighlight($id)
    {
        $announcement = $this->model->findOrFail($id);

        $announcement->highlighted_at = null;
        $announcement->save();
    }

    protected function setAllUnhighlighted()
    {
        $this->model->whereNotNull('highlighted_at')->update(['highlighted_at' => null]);

        return true;
    }

    public function getHighlighted()
    {
        return $this->model->whereNotNull('highlighted_at')->get();
    }

    public function all($columns = ['*'])
    {
        $this->applyCriteria();
        $this->applyScope();

        if (!$this->allowedCache('all') || $this->isSkippedCache()) {

            return $this->model->orderBy('highlighted_at', 'desc')
                ->orderBy('created_at', 'desc')
                ->get($columns);
        }

        $key = $this->getCacheKey('all', func_get_args());
        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($columns) {
            return $this->model->orderBy('highlighted_at', 'desc')
                ->orderBy('created_at', 'desc')
                ->get($columns);
        });

        $this->resetModel();
        $this->resetScope();
        return $value;
    }

    public function paginate($limit = null, $columns = ['*'], $method = 'paginate')
    {
        $this->applyCriteria();
        $this->applyScope();


        if (!$this->allowedCache('paginate') || $this->isSkippedCache()) {
            return $this->model->orderBy('highlighted_at', 'desc')
                ->orderBy('created_at', 'desc')
                ->paginate($limit, $columns, $method);
        }

        $key = $this->getCacheKey('paginate', func_get_args());

        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($limit, $columns, $method) {
            return $this->model->orderBy('highlighted_at', 'desc')
                ->orderBy('created_at', 'desc')
                ->paginate($limit, $columns, $method);
        });

        $this->resetModel();
        $this->resetScope();
        return $value;
    }
}