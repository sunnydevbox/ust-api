<?php

namespace Sunnydevbox\UST\Repositories\User;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Sunnydevbox\UST\Models\Alumnus;
use Sunnydevbox\UST\Models\Announcement;
use Sunnydevbox\UST\Models\ClinicAddress;
use Sunnydevbox\UST\Models\Device;
use Sunnydevbox\UST\Models\HospitalAffiliation;
use Sunnydevbox\UST\Models\Profile;
use Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Spatie\Permission\Models\Role;
use Sunnydevbox\UST\Notifications\AnnouncementNotification;

class UserRepository extends \Sunnydevbox\TWUser\Repositories\User\UserRepository
{
    protected $fieldSearchable = [

        'id',
        'first_name' => 'like',
        'last_name' => 'like',
        'middle_name' => 'like'

    ];

    //use TWMetaTrait;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return config('ust.model_user');
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        //return "\Sunnydevbox\UST\Validators\UserValidator";
    }



    public function register($request)
    {
        $requestCollection = collect($request->all());

        $model = $this->makeModel();

        if (method_exists($model, 'metaFields')) {
            foreach ($model->metaFields() as $field) {
                $request->offsetUnset($field);
            }
        }
        //$this->makeValidator()->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
        if ($this->checkDuplicateEmail($request->get('email'))) {
            throw new \Exception('Duplicate email', 400);
        } elseif (! $this->checkUniqueUser($request->get('first_name'), $request->get('last_name'), $request->get('batch'))) {
            throw new \Exception('Duplicate User', 400);
        } elseif(! $this->checkValidAlumni($request->get('first_name'), $request->get('last_name'), $request->get('batch'))){
            throw new \Exception("Invalid Alumni Credentials", 400);

        }else {
            $creds = $request->all();

            $creds['verification_token'] = $this->setVerificationToken();
            $creds['is_verified'] = null;

            $result = $this->create($creds);

            //assign alumni role
            $role = Role::where('name', 'alumni')->first();

            $result->assignRole($role);

            return $result;
        }
    }

    public function checkValidAlumni($fname, $lname, $batch)
    {
        return (Alumnus::whereRaw("LOWER(`first_name`) = '".strtolower($fname)."'")->whereRaw("LOWER(`last_name`) = '".strtolower($lname)."'")->where('batch', $batch)->first()) ? true : false;
    }

    public function checkUniqueUser($fname, $lname, $batch, $user = null)
    {
        if($user){
            return $this->makeModel()->whereRaw("LOWER(`first_name`) = '".strtolower($fname)."'")->whereRaw("LOWER(`last_name`) = '".strtolower($lname)."'")->where('batch', $batch)->where('id', '!=', $user->id)->first() ? false : true;
        } else {
            return $this->makeModel()->whereRaw("LOWER(`first_name`) = '".strtolower($fname)."'")->whereRaw("LOWER(`last_name`) = '".strtolower($lname)."'")->where('batch', $batch)->first() ? false : true;
        }
    }


    public function checkDuplicateEmail($email)
    {
        return $this->makeModel()->where('email', $email)->first() ? true : false;
    }




    public function updateAccount($user, $request)
    {
        if (! Hash::check($request->get('password'), $user->password)) {
            throw  new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException('invalid_password');
        }

        // UPDATE PASSWORD
        $result = $this->update(['password' => $request->get('new_password')], $user->id);

        return true;
    }


    public function verifyAccount($email, $token)
    {
        $email = trim(urldecode($email));

        $user = $this->model->where('email', '=', $email)->where('verification_token', $token)->first();

        return $user;
    }

    public function updateUserProfile($user, $request)
    {

        //check if new data does not conflict with existing user data
        if (! $this->checkUniqueUser($request->get('first_name'), $request->get('last_name'), $request->get('batch'), $user)) {
            //throw new \Exception('Duplicate User', 400);
        }

        $user_data = $request->only(['first_name', 'middle_name', 'last_name', 'batch']);
        $profile_data = $request->only([
            'profile_image',
            'street_no',
            'street',
            'city',
            'province',
            'country_code',
            'postal_code',
            'mobile',
            'specialty_id',
            'specialty_others',
            'sub_specialty_id',
            'sub_specialty_others',
            'sub_specialty_id_2',
            'sub_specialty_2_others',
            'position',
            'society_affiliation',
            'affiliation_address',
            'license_no',
            'is_info_shareable',
            'is_email_clinical_shareable'
        ]);

        //check if specialty (including sub specialties and others)
        if(($profile_data['specialty_id'] && $profile_data['specialty_others']) ||
            ($profile_data['sub_specialty_id'] && $profile_data['sub_specialty_others']) ||
            ($profile_data['sub_specialty_id_2'] && $profile_data['sub_specialty_2_others'])
        ){
            throw new \Exception("Invalid specialty data.", 400);
        }

        $hospital_affiliations = $request->get('hospital_affiliations', []);
        $clinical_addresses = $request->get('clinic_addresses', []);

        $user->fill($user_data);

        $profile = Profile::firstOrNew(['user_id' => $user->id]);
        $profile->fill($profile_data);

        $user->setRelation('profile', $profile);

        //hospital affiliations
        $affiliations = collect();
        foreach ($hospital_affiliations as $hospital_affiliation) {
            $id = isset($hospital_affiliation['id']) ? $hospital_affiliation['id'] : null;
            $affiliation = HospitalAffiliation::firstOrNew(['id' => $id]);
            $affiliation->fill($hospital_affiliation);
            $affiliations->push($affiliation);
        }

        $user->setRelation('hospital_affiliations', $affiliations);

        $addresses = collect();

        foreach ($clinical_addresses as $clinical_address) {

            $id = isset($clinical_address['id']) ? $clinical_address['id'] : null;

            $item = ClinicAddress::firstOrNew(['id' => $id]);
            $item->fill($clinical_address);
            $addresses->push($item);
        }

        $user->setRelation('clinic_addresses', $addresses);

        $user->push();

        //delete related models not in relations
        $user->hospital_affiliations()->whereNotIn('id', $affiliations->pluck('id'))->delete();
        $user->clinic_addresses()->whereNotIn('id', $addresses->pluck('id'))->delete();

        return true;
    }

    public static function addDevice(\SunnyDevbox\UST\Models\User $user, $advertising_id)
    {
        $device = Device::firstOrNew(['advertising_id' => $advertising_id]);
        return $user->devices()->save($device);
    }

    public function notifyUsers(Announcement $announcement)
    {
        foreach($this->model->all() as $user)
        {
            if($user->devices()->count() > 0){
                $user->notify(new AnnouncementNotification($announcement));
            }

        }
    }
}