<?php

namespace Sunnydevbox\UST;

use Illuminate\Support\ServiceProvider;
use Carbon\Carbon;
use Validator;

class USTServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application events.
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/api.php');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'ust');

        Validator::extend('batch', function($attribute, $value, $parameters, $validator) {
            $inserted = Carbon::createFromDate($value)->year;
            $since = $parameters[0];
            return $inserted >= $since && $inserted<= Carbon::now()->year;
        });

        $this->app['router']->aliasMiddleware('role', \Spatie\Permission\Middlewares\RoleMiddleware::class);
        $this->app['router']->aliasMiddleware('permission', \Spatie\Permission\Middlewares\PermissionMiddleware::class);
        $this->app['router']->aliasMiddleware('role_or_permission', \Spatie\Permission\Middlewares\RoleOrPermissionMiddleware::class);
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->registerPublisher();
        $this->registerProviders();
        $this->registerCommands();
    }

    protected function registerProviders()
    {
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();

        // EVENTS
        if (class_exists('\Sunnydevbox\TWUser\EventServiceProvider')
            && !$this->app->resolved('\Sunnydevbox\TWUser\EventServiceProvider')
        ) {
            $this->app->register(\Sunnydevbox\TWUser\EventServiceProvider::class);
        }


        if (class_exists('\Sunnydevbox\TWUser\TWUserServiceProvider')
            && !$this->app->resolved('\Sunnydevbox\TWUser\TWUserServiceProvider')
        ) {
            $this->app->register(\Sunnydevbox\TWUser\TWUserServiceProvider::class);
        }


        if (class_exists('\Intervention\Image\ImageServiceProvider')
            && !$this->app->resolved('\Intervention\Image\ImageServiceProvider')
        ) {
            $this->app->register(\Intervention\Image\ImageServiceProvider::class);
            $loader->alias('Image', \Intervention\Image\Facades\Image::class);
        }

        if (class_exists('\PragmaRX\Countries\Package\ServiceProvider')
            && !$this->app->resolved('\PragmaRX\Countries\Package\ServiceProvider')
        ) {
            $this->app->register(\PragmaRX\Countries\Package\ServiceProvider::class);
            $loader->alias('Countries', \PragmaRX\Countries\Package\Facade::class);
        }

        if (class_exists('\Sunnydevbox\TWCore\TWCoreServiceProvider')
            && !$this->app->resolved('\Sunnydevbox\TWCore\TWCoreServiceProvider')
        ) {
            $this->app->register(\Sunnydevbox\TWCore\TWCoreServiceProvider::class);
        }

        if (class_exists('\Spatie\Permission\PermissionServiceProvider')
            && !$this->app->resolved('\Spatie\Permission\PermissionServiceProvider')
        ) {
            $this->app->register(\Spatie\Permission\PermissionServiceProvider::class);
        }

        if (class_exists('\Bnb\Laravel\Attachments\AttachmentsServiceProvider')
            && !$this->app->resolved('\Bnb\Laravel\Attachments\AttachmentsServiceProvider')
        ) {
            $this->app->register(\Bnb\Laravel\Attachments\AttachmentsServiceProvider::class);
            $this->app->bind(
                \Bnb\Laravel\Attachments\Contracts\AttachmentContract::class,
                \Sunnydevbox\UST\Models\Attachment::class
            );
        }

        $this->app->register(\Sunnydevbox\UST\EventServiceProvider::class);
        $this->app->register(\Maatwebsite\Excel\ExcelServiceProvider::class);
        $this->app->register(\NotificationChannels\FCM\ServiceProvider::class);

        $loader->alias('Excel', \Maatwebsite\Excel\Facades\Excel::class);
        $loader->alias('FCM', \LaravelFCM\Facades\FCM::class);
        $loader->alias('FCMGroup', \LaravelFCM\Facades\FCMGroup::class);
    }


    protected function registerCommands()
    {
        if ($this->app->runningInConsole()) {

            $this->commands([
                // \Sunnydevbox\UST\Console\Commands\PublishMigrationsCommand::class,
                \Sunnydevbox\UST\Console\Commands\PublishConfigCommand::class,
                \Sunnydevbox\UST\Console\Commands\SetupCommand::class,
                \Sunnydevbox\UST\Console\Commands\MigrateCommand::class,
                \Sunnydevbox\UST\Console\Commands\SeedCommand::class
            ]);

            /*$localViewFactory = $this->createLocalViewFactory();
            $this->app->singleton(
                'command.sleepingowl.ide.generate',
                function ($app) use ($localViewFactory) {
                    return new \SleepingOwl\Admin\Console\Commands\GeneratorCommand($app['config'], $app['files'], $localViewFactory);
                }
            );

            $this->commands('command.sleepingowl.ide.generate');*/
        }
    }


    protected function registerPublisher()
    {
         $this->publishes([
            __DIR__ . '/../config/config.php' => config_path('ust.php'),
        ], 'config');


        $this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/ust/'),
        ]);

        $this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/ust/'),
        ]);

        $this->mergeConfigFrom(
            __DIR__ . '/../config/config.php', 'ust'
        );

    }


}