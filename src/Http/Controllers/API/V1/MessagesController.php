<?php

namespace Sunnydevbox\UST\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use http\Exception\InvalidArgumentException;
use Illuminate\Container\Container as Application;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Sunnydevbox\UST\Repositories\Message\MessageRepository;
use Sunnydevbox\UST\Repositories\Photos\PhotosRepository;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use JWTAuth;
use Sunnydevbox\UST\Repositories\User\UserRepository;
use Sunnydevbox\UST\Services\MessageService;
use Sunnydevbox\UST\Transformers\MessageTransformer;
use Sunnydevbox\UST\Transformers\SpecialtiesTransformer;
use Sunnydevbox\UST\Validators\MessageValidator;
use Auth;
use Exception;

class MessagesController extends APIBaseController
{
    protected $repository;

    protected $userRepository;

    protected $transformer;

    protected $service;

    protected $validator;

    public function __construct(
        MessageRepository $repository,
        UserRepository $userRepository,
        MessageTransformer $transformer,
        MessageService $service,
        MessageValidator $validator
    ) {
        $this->repository = $repository;
        $this->transformer = $transformer;
        $this->service = $service;
        $this->validator = $validator;
        $this->userRepository = $userRepository;
    }

    public function store(Request $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            if (Auth::user()->id == $request->get('receiver_id')) {
                throw new Exception('Invalid Request.');
            }

            $receiver = $this->userRepository->find($request->get('receiver_id'));
            $this->service->send(Auth::user(), $receiver, $request->all());

            return response()->json([], 201);
        } catch (ValidatorException $e) {
            return response()->json([
                'status_code' => 400,
                'message' => $e->getMessageBag(),
            ], 400);
        }
    }

    public function getMessages(Request $request, $user_id)
    {
        $receiver = $this->userRepository->find($user_id);
        $messages = $this->repository->getMessages(Auth::user(), $receiver);

        return $this->response->paginator($messages, new MessageTransformer);
    }
}