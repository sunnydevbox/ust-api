<?php

namespace Sunnydevbox\UST\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Sunnydevbox\UST\Repositories\User\AnnouncementRepository;
use Sunnydevbox\UST\Requests\FileRequest;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\TWUser\Transformers\PermissionTransformer;
use Sunnydevbox\TWUser\Transformers\RoleTransformer;
use JWTAuth;
use Sunnydevbox\UST\Transformers\AnnouncementTransformer;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Input;
use Prettus\Validator\Contracts\ValidatorInterface;
use Sunnydevbox\UST\Events\UserRegisteredEvent;
use Sunnydevbox\TWUser\Events\UserVerifiedEmailEvent;
use Sunnydevbox\UST\Transformers\UserTransformer;
use Auth;

class AnnouncementsController extends APIBaseController
{
    protected $repository;
    protected $transformer;

    public function __construct(AnnouncementRepository $repository, AnnouncementTransformer $transformer)
    {
        $this->repository = $repository;
        $this->transformer = $transformer;
    }

    public function index(Request $request)
    {
        $limit = is_numeric($request->get('limit')) ? $request->get('limit') : config('repository.pagination.limit', 15);
        $filter = $request->get('filter');

        if($filter == 'highlighted'){
            $result = $this->repository->getHighlighted();

            return $this->response()
                ->collection($result, $this->transformer)
                ->withHeader('Content-Range', $result->count());
        }

        else {

            if ($limit == 0) {

                $result = $this->repository->all();

                return $this->response()
                    ->collection($result, $this->transformer)
                    ->withHeader('Content-Range', $result->count());
            } else {

                $result = $this->repository->paginate($limit);

                return $this->response
                    ->paginator($result, $this->transformer)
                    ->withHeader('Content-Range', $result->total());
            }
        }
    }


    public function show($id)
    {
        try{
            $announcement = $this->repository->viewAnnouncement($id);


        } catch (\Exception $e){
            throw new \Exception($e->getMessage(), ($e->getCode()) ? $e->getCode() : 500);
        }

        return $this->response->item($announcement, new $this->transformer);
    }

   
}