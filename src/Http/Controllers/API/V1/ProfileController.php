<?php

namespace Sunnydevbox\UST\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use JWTAuth;
use Auth;
use Sunnydevbox\UST\Repositories\Profile\ProfileRepository; 
use Sunnydevbox\UST\Transformers\ProfileTransformer;

class ProfileController extends APIBaseController
{
    public function __construct(
        ProfileRepository $repository, 
        ProfileTransformer $transformer
    ) {
        $this->repository = $repository;
        $this->transformer = $transformer;
    }
}