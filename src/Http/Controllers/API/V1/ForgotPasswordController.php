<?php

namespace Sunnydevbox\UST\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use Sunnydevbox\UST\Events\UserRequestResetPasswordEvent;
use Illuminate\Foundation\Validation\ValidatesRequests;


class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controllerb
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails, ValidatesRequests;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('guest');
    }


    public function sendResetLinkEmail(Request $request)
    {

        $this->validate($request, ['email' => 'required|email']);
        if ($request->wantsJson()) {
            $User = config('auth.providers.users.model');
            
            $user = $User::where('email', $request->input('email'))->first();
            if (!$user) {
                //dd(trans('passwords.user'));
                return response()->json([
                    'message' => trans('passwords.user')
                ], 400);
            }
            
            event(new UserRequestResetPasswordEvent($user));

            // No need to return anything
            // if all went well
            return response([], 200);
        }

    }
}
