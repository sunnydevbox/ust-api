<?php

namespace Sunnydevbox\UST\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Sunnydevbox\UST\Repositories\User\AnnouncementRepository;
use Sunnydevbox\UST\Repositories\Specialties\SpecialtiesRepository;
use Sunnydevbox\UST\Requests\FileRequest;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\TWUser\Transformers\PermissionTransformer;
use Sunnydevbox\TWUser\Transformers\RoleTransformer;
use JWTAuth;
use Sunnydevbox\UST\Transformers\AnnouncementTransformer;
use Sunnydevbox\UST\Transformers\SpecialtiesTransformer;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Input;
use Prettus\Validator\Contracts\ValidatorInterface;
use Sunnydevbox\UST\Events\UserRegisteredEvent;
use Sunnydevbox\TWUser\Events\UserVerifiedEmailEvent;
use Sunnydevbox\UST\Transformers\UserTransformer;
use Auth;

class SpecialtiesController extends APIBaseController
{
    protected $repository;
    protected $transformer;

    public function __construct(SpecialtiesRepository $repository, SpecialtiesTransformer $transformer)
    {
        $this->repository = $repository;
        $this->transformer = $transformer;
    }

    public function index(Request $request)
    {
        try{
            $specialties = $this->repository->getAllSpecialties();

        } catch (\Exception $e){
            throw new \Exception($e->getMessage(), ($e->getCode()) ? $e->getCode() : 500);
        }

        return $this->response->collection($specialties, new $this->transformer);
    }
}