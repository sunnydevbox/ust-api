<?php

namespace Sunnydevbox\UST\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\UST\Services\AggregateService;

class AggregateController extends APIBaseController
{
    public function __construct(
        AggregateService $service
    ) {
       $this->service = $service;
    }

    public function index(Request $request)
    {
        $lists = ($request->get('list')) ? explode(';', $request->get('list')) : [];

        $result = $this->service->aggregate($lists);
        
        return response()->json($result);
    }
}