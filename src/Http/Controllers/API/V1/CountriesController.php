<?php

namespace Sunnydevbox\UST\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use JWTAuth;
use Auth;
use Sunnydevbox\UST\Transformers\CountryTransformer;

class CountriesController extends APIBaseController
{
    protected $repository;

    protected $transformer;

    public function __construct(\Sunnydevbox\UST\Repositories\Address\CountryRepository $repository, CountryTransformer $transformer)
    {
        $this->repository = $repository;
        $this->transformer = $transformer;
    }

    public function index(Request $request)
    {

        $result = $this->repository->getAllCountries($request);

        return $this->response()->collection($result, $this->transformer)->withHeader('Content-Range', $result->count());
    }

    public function show($id)
    {
        try {
            $announcement = $this->repository->viewAnnouncement($id);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), ($e->getCode()) ? $e->getCode() : 500);
        }

        return $this->response->item($announcement, new $this->transformer);
    }
}