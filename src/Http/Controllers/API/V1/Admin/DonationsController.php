<?php

namespace Sunnydevbox\UST\Http\Controllers\API\V1\Admin;

use Dingo\Api\Http\Request;
use Sunnydevbox\UST\Repositories\Specialties\DonationsRepository;
use Sunnydevbox\UST\Repositories\User\AnnouncementRepository;
use Sunnydevbox\UST\Transformers\DonationsTransformer;
use Sunnydevbox\UST\Validators\DonationValidator;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class DonationsController extends APIBaseController
{
    protected $repository;

    protected $validator;

    protected $transformer;

    public function __construct(DonationsRepository $repository, DonationsTransformer $transformer, DonationValidator $validator)
    {
        $this->repository = $repository;
        $this->transformer = $transformer;
        $this->validator = $validator;
    }

    public function index(Request $request)
    {
        $result = $this->repository->paginate();

        return $this->response()->paginator($result, $this->transformer);
    }

    public function store(Request $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $donation = $this->repository->create($request->all());

            return $this->response->item($donation, $this->transformer);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => $e->getMessage()], 500);
        } catch (ValidatorException $e) {

            return response()->json([
                'status_code' => 400,
                'message' => $e->getMessageBag(),
            ], 400);
        }
    }
}