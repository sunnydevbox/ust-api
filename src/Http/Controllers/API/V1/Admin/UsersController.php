<?php

namespace Sunnydevbox\UST\Http\Controllers\API\V1\Admin;

use Dingo\Api\Http\Request;
use Prettus\Validator\Exceptions\ValidatorException;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\TWUser\Transformers\PermissionTransformer;
use Sunnydevbox\TWUser\Transformers\RoleTransformer;
use JWTAuth;
use Sunnydevbox\UST\Models\Alumni;
use Sunnydevbox\UST\Models\Alumnus;
use Sunnydevbox\UST\Repositories\User\AlumnusRepository;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Input;
use Prettus\Validator\Contracts\ValidatorInterface;
use Sunnydevbox\UST\Events\UserRegisteredEvent;
use Sunnydevbox\TWUser\Events\UserVerifiedEmailEvent;
use Sunnydevbox\UST\Transformers\UserTransformer;
use Auth;
use Validator;
use Excel;

class UsersController extends \Sunnydevbox\TWUser\Http\Controllers\API\V1\UserController
{
    protected $return_messages = [
        'object_not_found' => 'User does not exist',
        '400' => 'Invalid request',
        'success' => 'Operation successful.',
    ];

    public function __construct(
        \Sunnydevbox\UST\Repositories\User\UserRepository $repository,
        \Sunnydevbox\UST\Validators\UserValidator $validator,
        UserTransformer $transformer
    ) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

    public function show($id)
    {
        $user = $this->repository->find($id);

        return $this->response->item($user, $this->transformer);
    }

    public function update(Request $request, $id)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $user = $this->repository->find($id);

            $this->repository->updateUserProfile($user, $request);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => $e->getMessage()], 500);
        } catch (ValidatorException $e) {
            return response()->json([
                'status_code' => 400,
                'message' => $e->getMessageBag(),
            ], 400);
        }

        return $this->response->item($user, $this->transformer);
    }

    public function import(Request $request, AlumnusRepository $alumnusRepository)
    {
        try{
            $validator = Validator::make($request->all(), [
                'file' => 'required|file'
            ]);

            if ($validator->fails()) {
                throw new ValidatorException($validator->errors());
            }
            $file = $request->file('file');
            Excel::load($file, function($reader) use ($alumnusRepository){
                foreach( $reader->all() as $row){
                    $r = $row->toArray();

                    if ($r['first_name'] != '' && $r['last_name'] != '') {
                        $alumnus = $alumnusRepository->saveOrUpdateAlumni($row);
                    }
                }
            });

            // return response()->json(['status' => 'successful']);

        } catch (ValidatorException $e){
            return response()->json([
                'status_code' => 400,
                'message' => $e->getMessageBag(),
            ], 400);
        } catch (Exception $e){
           return response()->json([
                'status_code' => 500,
                'message' => $e->getMessageBag(),
            ], 500);
        }


    }
}