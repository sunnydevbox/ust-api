<?php

namespace Sunnydevbox\UST\Http\Controllers\API\V1\Admin;

use Dingo\Api\Http\Request;
use Sunnydevbox\UST\Repositories\User\AnnouncementRepository;
use Sunnydevbox\UST\Repositories\User\UserRepository;
use Sunnydevbox\UST\Requests\FileRequest;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\TWUser\Transformers\PermissionTransformer;
use Sunnydevbox\TWUser\Transformers\RoleTransformer;
use JWTAuth;
use Sunnydevbox\UST\Transformers\AnnouncementTransformer;
use Sunnydevbox\UST\Validators\AnnouncementValidator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Input;
use Prettus\Validator\Contracts\ValidatorInterface;
use Sunnydevbox\UST\Events\UserRegisteredEvent;
use Sunnydevbox\TWUser\Events\UserVerifiedEmailEvent;
use Sunnydevbox\UST\Transformers\UserTransformer;
use Auth;
use Prettus\Validator\Exceptions\ValidatorException;
use LaravelFCM\Message\Topics;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use Illuminate\Container\Container as Application;
use Sunnydevbox\UST\Services\AnnouncementService;

class AnnouncementsController extends APIBaseController
{
    protected $repository;
    protected $transformer;

    public function __construct(
        AnnouncementRepository $repository, 
        AnnouncementTransformer $transformer, 
        AnnouncementValidator $validator,
        AnnouncementService $service
    ) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
        $this->service = $service;
    }

    public function show($id)
    {
        try{
            $announcement = $this->repository->viewAnnouncement($id);


        } catch (\Exception $e){
            throw new \Exception($e->getMessage(), ($e->getCode()) ? $e->getCode() : 500);
        }

        return $this->response->item($announcement, new $this->transformer);
    }

    public function store(Request $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $result = $this->repository->saveAnnouncement($request);
            
            if ($image = $request->file('image')) {
                $attachment = $this->attachImage($request, $result->id);
            }

            // push notif for announcements

            (new UserRepository(new Application ))->notifyUsers($result);

            return $this->response->item($result, $this->transformer);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => $e->getMessage()], 500);
        } catch (ValidatorException $e) {

            return response()->json([
                'status_code' => 400,
                'message' => $e->getMessageBag(),
            ], 400);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $result = $this->repository->saveAnnouncement($request, $id);

            // TODO:: push notif for announcements

            return $this->response->item($result, $this->transformer);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => $e->getMessage()], 500);
        } catch (ValidatorException $e) {

            return response()->json([
                'status_code' => 400,
                'message' => $e->getMessageBag(),
            ], 400);
        }
    }

    public function destroy($id)
    {
        try {
            $result = $this->repository->delete($id);
            // TODO:: push notif for announcements
            return response()->json(['success' => true], 200);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function highlight($id)
    {
        try {
            $result = $this->repository->highlight($id);
            // TODO:: push notif for announcements
            return response()->json(['success' => true], 200);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function removeHighlight($id)
    {
        try {
            $result = $this->repository->removeHighlight($id);
            // TODO:: push notif for announcements
            return response()->json(['success' => true], 200);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function attachImage(Request $request, $id)
    {
        // MOVE THIS TO SERVICE !!!!!
        try {        
            $result = $this->service->attachImage($id, $request->file('image'));
            return  response()->json($result);
        } catch(\Excetption $e) {
            dd($e);
        }
        

        // return response()->json($attachment);
        
    }
}