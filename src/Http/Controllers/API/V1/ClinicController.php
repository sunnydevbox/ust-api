<?php

namespace Sunnydevbox\UST\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\UST\Services\ClinicService;
use Sunnydevbox\UST\Transformers\ClinicalAggregateTransformer;
use Sunnydevbox\UST\Repositories\Clinic\ClinicRepository;
use Sunnydevbox\UST\Transformers\ClinicalAddressesesTransformer;


class ClinicController extends APIBaseController
{
    public function __construct(
        ClinicService $service,
        ClinicalAggregateTransformer $clinicAggregateTransformer,
        ClinicRepository $repository,
        ClinicalAddressesesTransformer $transformer
    ) {
        $this->repository = $repository;
        $this->service = $service;
        $this->clinicAggregateTransformer = $clinicAggregateTransformer;
        $this->transformer = $transformer;
    }

    public function getCheckoutDetails(CheckoutRequest $request)
    {

        return response()->json($this->pay_service->doPrepareCheckout(http_build_query($request->all())));
    }

    public function aggregate(Request $request)
    {
        $result = $this->service->aggregate();
        
        return response()->json($result);
        return $this->response->item($result, $this->clinicAggregateTransformer);
    }
}