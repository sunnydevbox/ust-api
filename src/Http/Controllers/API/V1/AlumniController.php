<?php

namespace Sunnydevbox\UST\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use JWTAuth;
use Sunnydevbox\UST\Repositories\User\UserRepository;
use Sunnydevbox\UST\Transformers\AlumniTransformer;
use Sunnydevbox\UST\Transformers\AnnouncementTransformer;
use Auth;

class AlumniController extends APIBaseController
{
    protected $repository;

    protected $transformer;

    public function __construct(UserRepository $repository, AlumniTransformer $transformer)
    {
        $this->repository = $repository;
        $this->transformer = $transformer;
    }
    /*public function index(Request $request)
    {
        $limit = is_numeric($request->get('limit')) ? $request->get('limit') : config('repository.pagination.limit', 15);

        if ($limit == 0) {
            $result = $this->repository->all();

            return $this->response()
                ->collection($result, $this->transformer)
                ->withHeader('Content-Range', $result->count());
        } else {
            $result = $this->repository->paginate($limit);

            return $this->response
                ->paginator($result, $this->transformer)
                ->withHeader('Content-Range', $result->total());
        }
    }*/
    public function index(Request $request)
    {

        $limit = is_numeric($request->get('limit')) ? $request->get('limit') : config('repository.pagination.limit', 15);

        $result = $this->repository->allAlumni($request);

        return $this->response()->collection($result, $this->transformer)->withHeader('Content-Range', $result->count());
    }

    public function show($id)
    {
        try {
            $announcement = $this->repository->viewAnnouncement($id);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), ($e->getCode()) ? $e->getCode() : 500);
        }

        return $this->response->item($announcement, new $this->transformer);
    }
}