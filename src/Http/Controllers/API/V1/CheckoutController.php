<?php

namespace Sunnydevbox\UST\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Sunnydevbox\UST\Repositories\User\AnnouncementRepository;
use Sunnydevbox\UST\Requests\CheckoutRequest;
use Sunnydevbox\UST\Requests\FileRequest;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\TWUser\Transformers\PermissionTransformer;
use Sunnydevbox\TWUser\Transformers\RoleTransformer;
use JWTAuth;
use Sunnydevbox\UST\Services\PayService;
use Sunnydevbox\UST\Transformers\AnnouncementTransformer;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Input;
use Prettus\Validator\Contracts\ValidatorInterface;
use Sunnydevbox\UST\Events\UserRegisteredEvent;
use Sunnydevbox\TWUser\Events\UserVerifiedEmailEvent;
use Sunnydevbox\UST\Transformers\UserTransformer;
use Auth;

class CheckoutController extends APIBaseController
{
    protected $pay_service;

    public function __construct(PayService $pay_service)
    {
       $this->pay_service = $pay_service;
    }

    public function getCheckoutDetails(CheckoutRequest $request)
    {

        return response()->json($this->pay_service->doPrepareCheckout(http_build_query($request->all())));
    }
}