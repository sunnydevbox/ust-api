<?php

namespace Sunnydevbox\UST\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Prettus\Validator\Exceptions\ValidatorException;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\TWUser\Transformers\PermissionTransformer;
use Sunnydevbox\TWUser\Transformers\RoleTransformer;
use JWTAuth;
use Sunnydevbox\UST\Repositories\User\UserRepository;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Input;
use Prettus\Validator\Contracts\ValidatorInterface;
use Sunnydevbox\UST\Events\UserRegisteredEvent;
use Sunnydevbox\TWUser\Events\UserVerifiedEmailEvent;
use Sunnydevbox\UST\Transformers\UserTransformer;
use Sunnydevbox\UST\Services\UserService;
use Auth;
use Validator;

use Sunnydevbox\UST\Models\Alumnus;

class UsersController extends \Sunnydevbox\TWUser\Http\Controllers\API\V1\UserController
{
    protected $return_messages = [
        'object_not_found' => 'User does not exist',
        '400' => 'Invalid request',
        'success' => 'Operation successful.',
    ];

    public function __construct(
        \Sunnydevbox\UST\Repositories\User\UserRepository $repository,
        \Sunnydevbox\UST\Validators\UserValidator $validator,
        UserTransformer $transformer,
        UserService $service
    ) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
        $this->service = $service;
    }

    public function index(Request $request)
    {
        // IF filter is part of the query
        // then remove the default includes
        if ($request->get('filter')) {
            $this->transformer->setDefaultIncludes([]);
        }
        // dd($this->transformer->getDefaultIncludes());
        $this->repository->pushCriteria(\Sunnydevbox\UST\Criteria\AlumniUserCriteria::class);
        $this->repository->pushCriteria(\Sunnydevbox\UST\Criteria\AlumniSearchFilter::class);
        return parent::index($request);
    }

    public function show($id)
    {
        $this->repository->pushCriteria(\Sunnydevbox\UST\Criteria\AlumniUserCriteria::class);
        return parent::show($id);
    }

    public function index2(Request $request)
    {
        return parent::index($request);
    }
    
    public function show2($id)
    {
        return parent::show($id);
    }

    public function updatePassword(Request $request, $id)
    {
        $message = null;
        $code = null;

        try {
            $result = $this->service->updatePassword($id, $request->all());
            return response()->json(['status'=>'OK'], 200);
        } catch(ModelNotFoundException $e) {
            $message = 'User not found';
            $code = 400;
        }  catch(ValidatorException $e) {
            $message = $e->getMessageBag();
            $code = 400;
        }  catch(\Exception $e) {
            $message = $e->getMessage();
            $code = $e->getCode() ? $e->getCode() : 500;
        }

        return response()->json([
            'status_code'   => $code,
            'message' => $message,
        ], $code);
    }

    public function updateEmail(Request $request, $id)
    {
        $message = null;
        $code = null;

        try {
            $result = $this->service->updateEmail($id, $request->all());
            return response()->json(['status'=>'OK'], 200);
        } catch(ModelNotFoundException $e) {
            $message = 'User not found';
            $code = 400;
        }  catch(ValidatorException $e) {
            $message = $e->getMessageBag();
            $code = 400;
        }  catch(\Exception $e) {
            $message = $e->getMessage();
            $code = $e->getCode() ? $e->getCode() : 500;
        }

        return response()->json([
            'status_code'   => $code,
            'message' => $message,
        ], $code);
    }
    
    public function update2(Request $request, $id)
	{
        return parent::update($request, $id);
    }

    public function update(Request $request, $id)
	{
        $message = null;
        $code = null;

        try { 
            $result = $this->service->updateAlumni($request->all(), $id);

            $Re = $this->repository
                    ->with(['hospital_affiliations','profile'])
                    ->find($id);
            $Re->makeVisible('updated_at');

            return response()->json($Re);

        } catch(ModelNotFoundException $e) {
            $message = 'User not found';
            $code = 400;
        }  catch(ValidatorException $e) {
            $message = $e->getMessageBag();
            $code = 400;
        }  catch(\Exception $e) {
            $message = $e->getMessage();
            $code = $e->getCode() ? $e->getCode() : 500;
        }

        return response()->json([
            'status_code'   => $code,
            'message' => $message,
        ], $code);

	}

    public function validate(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'batch' => 'required|numeric|batch:1900',
            ]);

            if (! $this->repository->checkValidAlumni($request->get('first_name'), $request->get('last_name'), $request->get('batch'))) {
                $valid = false;
                $message = "No Alumni Found.";
            } elseif (! $this->repository->checkUniqueUser($request->get('first_name'), $request->get('last_name'), $request->get('batch'))) {
                $valid = false;
                $message = "Alumni Already Registered.";
            } else {
                $valid = true;
                $message = "Alumni Details Found";
            }

            return response()->json(['status' => $valid, 'message' => $message], 200);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => $e->getMessage()], 500);
        } catch (ValidatorException $e) {

            return response()->json([
                'status_code' => 400,
                'message' => $e->getMessageBag(),
            ], 400);
        }
    }

    public function register(Request $request)
    {

        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $result = $this->repository->register($request);

            // NOTIFY USER
            event(new UserRegisteredEvent($result));

            return $this->response->item($result, $this->transformer);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => $e->getMessage()], 500);
        } catch (ValidatorException $e) {

            return response()->json([
                'status_code' => 400,
                'message' => $e->getMessageBag(),
            ], 400);
        }
    }

    public function authenticate(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }

            $user = JWTAuth::authenticate($token);

            if ($user->is_verified) {
                $user->token = JWTAuth::getToken()->get();

                //register device if advertising_id is sent
                if ($this->loginHasDeviceId($request)) {
                    UserRepository::addDevice(Auth::user(), $request->get('advertising_id'));
                }
                
                // Load the roles and set default include in transformer
                $user->roles;
                $this->transformer->setDefaultIncludes(['roles']);
                
                return $this->response->item($user, $this->transformer);
            } else {
                return $this->response->errorUnauthorized('not_verified');
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        } catch (ValidatorException $e) {
            return response()->json([
                'status_code' => 400,
                'message' => $e->getMessageBag(),
            ], 400);
        }

        // all good so return the token
        return response()->json(compact('token'));
    }

    public function logout()
    {
        try {
            auth()->logout();
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function showProfile()
    {
        $user = Auth::user();

        return $this->response->item($user, $this->transformer);
    }

    public function updateProfile(Request $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $user = Auth::user();

            $this->repository->updateUserProfile($user, $request);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => $e->getMessage()], 500);
        } catch (ValidatorException $e) {
            return response()->json([
                'status_code' => 400,
                'message' => $e->getMessageBag(),
            ], 400);
        }

        return $this->response->item($user, $this->transformer);
    }

    public function updateAccount(Request $request)
    {

        try {
            //$this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_ACCOUNT_UPDATE);

            // VALIDATION
            $validator = Validator::make($request->all(), [
                'password' => 'required',
                'new_password' => 'required|min:8|confirmed',
                'new_password_confirmation' => 'required|min:8',
            ]);

            if ($validator->fails()) {
                throw new \Dingo\Api\Exception\UpdateResourceFailedException('Failed to update password', $validator->errors());
            }

            $user = Auth::user();

            $this->repository->updateAccount($user, $request);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => $e->getMessage()], 500);
        } catch (ValidatorException $e) {
            return response()->json([
                'status_code' => 400,
                'message' => $e->getMessageBag(),
            ], 400);
        }

        return $this->response->item($user, $this->transformer);
    }

    /**
     * @param \Dingo\Api\Http\Request $request
     * @return bool
     */
    private function loginHasDeviceId(Request $request)
    {
        return $request->has('advertising_id');
    }


    public function resendActivation(Request $request)
    {
        $result = $this->service->resendActivation($request->get('email'));

        if ($result) {
            return response()->json([
                'status' => 'ok',
                'code' => 200]);
        }
    }




    public function parse(Request $request) 
    {
        echo '<pre>';
        foreach(range(1902, 1903) as $year ) {
            $filename = "Copy of USTMAA DATABASE (1955 to 2018) .xlsx - {$year}.csv";
            $file = storage_path("app/csv/{$filename}");
            
            if (!file_exists($file)) continue;

            $handle = fopen($file, "r");
            $header = true;

            
            $year = 1972;
            
            if ($handle) {
                var_dump("*************************************************************");
                var_dump("**************************** {$year} ************************");
                var_dump("* {$filename} * ");
                var_dump("*************************************************************");
                while ($row = 
                    fgetcsv($handle, 1000, ",")) {
                    if ($header) {
                        $header = false;
                        /* array:4 [▼ 0 => "SURNAME" 1 => "GIVEN NAME" 2 => "MIDDLE NAME" 3 => "SUFFIX" ] */
                
                    } else {
                        if (strlen($row[0]) && strlen($row[1])) {
                            //echo "<br> {$row[0]}, {$row[1]}";
                            // dd(User::first());
                            // 1) FIND MATCHES from ALUMNI table
                    
                            $alumni = new Alumnus;
                            $alumni = $alumni->where('last_name', 'like', "%{$row[0]}%")
                                    ->where('first_name', 'like', "%{$row[1]}%")
                                    ->where('batch', $year)
                                    ->get();
                    
                            $alumniData = [
                                'last_name' => ucwords(strtolower(trim($row[0]))),
                                'first_name' => ucwords(strtolower(trim($row[1]))),
                                'middle_name' => ucwords(strtolower(trim($row[2]))),
                                'suffix'  =>  ucwords(strtolower(trim($row[3]))),
                                'batch' => $year,
                            ];
                            
                            if ($alumni->count() == 1) {
                                $alumni->first()->update($alumniData);
                                echo "... Match found ... updating a record";
                                //var_dump($alumni->first());
                            } else if ($alumni->count() > 1) { 
                                echo "... Matches found ... updating a record";
                            } else {
                                // no match so create a record
                                echo "... No match so we're creating a record";
                                // $data['email'] = '__'.$row[0].'_'.$row[1].'_'.$year.'_'.(rand(0,999) * rand(10000,19999)) . '@ustmaa.org';
                                // $data['claimed'] = 0;
                                // var_dump($data);
                                Alumnus::create($alumniData);
                            }
                        }
                    }
                    
                }
                fclose($handle);
        
            } else {
                echo 'NOT VALUD ' . $filename;
            }
        }
        
    }
}