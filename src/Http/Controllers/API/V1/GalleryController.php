<?php

namespace Sunnydevbox\UST\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\UST\Repositories\Gallery\GalleryRepository; 
use Sunnydevbox\UST\Transformers\GalleryTransformer;
use Sunnydevbox\UST\Services\GalleryService;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class GalleryController extends APIBaseController
{
    public function attachFile(Request $request)
    {
        $message = null;
        $code = null;

        try {
            $result = $this->service->attachFile($request->all());
            return response()->json($result);
        } catch(ModelNotFoundException $e) {
            $message = 'Gallery not found';
            $code = 400;
        }  catch(ValidatorException $e) {
            $message = $e->getMessageBag();
            $code = 400;
        }  catch(\Exception $e) {
            $message = $e->getMessage();
            $code = $e->getCode() ? $e->getCode() : 500;
        }

        return response()->json([
            'status_code'   => $code,
            'message' => $message,
        ], $code);
    }

    public function detachFile(Request $request)
    {
        $message = null;
        $code = null;

        try {
            $result = $this->service->detachFile($request->all());
            return $this->response->noContent();
        } catch(ModelNotFoundException $e) {
            $message = 'Gallery not found';
            $code = 400;
        }  catch(ValidatorException $e) {
            $message = $e->getMessageBag();
            $code = 400;
        }  catch(\Exception $e) {
            $message = $e->getMessage();
            $code = $e->getCode() ? $e->getCode() : 500;
        }

        return response()->json([
            'status_code'   => $code,
            'message' => $message,
        ], $code);
    }

    public function __construct(
        GalleryRepository $repository, 
        GalleryTransformer $transformer,
        GalleryService $service
    ) {
        $this->repository = $repository;
        $this->transformer = $transformer;
        $this->service = $service;
    }
}