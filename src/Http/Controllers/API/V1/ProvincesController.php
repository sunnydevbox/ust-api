<?php

namespace Sunnydevbox\UST\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use JWTAuth;
use Auth;
use Sunnydevbox\UST\Repositories\Address\ProvinceRepository;
use Sunnydevbox\UST\Transformers\ProvinceTransformer;

class ProvincesController extends APIBaseController
{
    protected $repository;

    protected $transformer;

    public function __construct(ProvinceRepository $repository, ProvinceTransformer $transformer)
    {
        $this->repository = $repository;
        $this->transformer = $transformer;
    }

    public function index(Request $request)
    {

        $result = ($request->get('country_id') )? $this->repository->allByCountry($request->get('country_id')) : $this->repository->all();

        return $this->response()->collection($result, $this->transformer)->withHeader('Content-Range', $result->count());
    }

    public function show($id)
    {
        try {
            $announcement = $this->repository->viewAnnouncement($id);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), ($e->getCode()) ? $e->getCode() : 500);
        }

        return $this->response->item($announcement, new $this->transformer);
    }
}