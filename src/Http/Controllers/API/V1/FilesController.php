<?php

namespace Sunnydevbox\UST\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Illuminate\Support\Facades\Storage;
use Sunnydevbox\UST\Requests\FileRequest;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\TWUser\Transformers\PermissionTransformer;
use Sunnydevbox\TWUser\Transformers\RoleTransformer;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Input;
use Prettus\Validator\Contracts\ValidatorInterface;
use Sunnydevbox\UST\Events\UserRegisteredEvent;
use Sunnydevbox\TWUser\Events\UserVerifiedEmailEvent;
use Sunnydevbox\UST\Transformers\UserTransformer;
use Auth;


class FilesController extends APIBaseController
{
    protected $repository;

    public function upload(FileRequest $request)
    {
        $result = $this->service->upload($request->all());

        return $this->response->array($result);
    }

    public function getImage($file, $type)
    {
     	$img = \Image::cache(function($image) use ($file, $type) {
//              dd(storage_path('app/public/storage_path('app/public/profile/'.$fileprofile/'.$file));
          return $image->make(storage_path('app/public/profile/'.$file))->resize(400, null, function ($constraint) {
                $constraint->aspectRatio();
            });
        },10, true);

	    return $img->response();
    }

    public function __construct(
        \Sunnydevbox\UST\Services\FileService $service
    ) {
        $this->service = $service;
    }
}