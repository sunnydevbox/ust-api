<?php

namespace Sunnydevbox\UST\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\UST\Repositories\Gallery\GalleryPhotoRepository; 
use Sunnydevbox\UST\Transformers\GalleryPhotoTransformer;
use Sunnydevbox\UST\Services\GalleryPhotoService;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class GalleryPhotoController extends APIBaseController
{   

    public function attachFile(Request $request)
    {
        $message = null;
        $code = null;

        try {
            $result = $this->service->attachFile($request->all());
            return response()->json($result);
        } catch(ModelNotFoundException $e) {
            $message = 'Gallery not found';
            $code = 400;
        }  catch(ValidatorException $e) {
            $message = $e->getMessageBag();
            $code = 400;
        }  catch(\Exception $e) {
            $message = $e->getMessage();
            $code = $e->getCode() ? $e->getCode() : 500;
        }

        return response()->json([
            'status_code'   => $code,
            'message' => $message,
        ], $code);
    }

    public function destroy($id)
    {
        $result = $this->service->detachPhoto($id);

        return $result;
    }


    public function __construct(
        GalleryPhotoRepository $repository, 
        GalleryPhotoTransformer $transformer,
        GalleryPhotoService $service
    ) {
        $this->repository = $repository;
        $this->transformer = $transformer;
        $this->service = $service;
    }
}