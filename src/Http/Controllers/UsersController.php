<?php

namespace Sunnydevbox\UST\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sunnydevbox\UST\Repositories\User\UserRepository;
use Sunnydevbox\UST\Events\UserVerifiedEmailEvent;

class UsersController extends Controller
{
    protected $repository;

    public function __construct(UserRepository $userRepository)
    {
        $this->repository = $userRepository;
    }

    public function verify($email, $token)
    {
        $error = null;
        $user = $this->repository->verifyAccount($email, $token);

        if (!$user) {
            $error = 'Invalid Token';            
            // throw new \Exception('Invalid token.', 500);
        } else {
            event(new UserVerifiedEmailEvent($user));
        }

        return response()->view('ust::user.verified', ['user' => $user, 'error' => $error]);
    }
}
