<?php

namespace Sunnydevbox\UST\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use JWTAuth;

use \Carbon\Carbon;
use Dingo\Api\Http\Request;

//use Illuminate\Http\Request;

/**
 * Class AlumniSearchFilter.
 *
 * @package namespace App\Criteria;
 */
class AlumniUserCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {

        $user = \Auth::user();
        $isAlumni = $user->hasRole('alumni');

        if ($isAlumni) {
            /**
             * IF current user has an ALUMNI role,
             * he can only view other active users
             * EXCEPT when it is his own profile he is viewing
             */
            /** 
             * View the user as an object
             */
            if (request()->route('user') && ($user->id != (int)request()->route('user'))) {
                $model = $model->active();
            }

            // RETURN the fields based on permission
        }
        
        return $model;
    }
}
