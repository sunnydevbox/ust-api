<?php

namespace Sunnydevbox\UST\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use JWTAuth;

use \Carbon\Carbon;
use Dingo\Api\Http\Request;

//use Illuminate\Http\Request;

/**
 * Class AlumniSearchFilter.
 *
 * @package namespace App\Criteria;
 */
class AlumniSearchFilter implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $alumniSearchFilters = request()->get('alumniSearchFilters');
        $user = \Auth::user();

        $isAlumni = $user->hasRole('alumni');
        $isAdmin = $user->hasRole('admin');

        // If requestor is an alumni 
        // then restrict result to alumni only
        if ($isAlumni) {
            $model = $model->role('alumni');

            // Exclude himself
            $model = $model->where('id', '!=', $user->id);

            // Exclude users with admin roles
            // ****** TO DO
            // $model = $model->where('roles', function)
                

            // ONLY active users
            $model = $model->active();
        }


        // DATE RANGE
        if ($alumniSearchFilters) {
            $kv = explode(';', $alumniSearchFilters);
            
            foreach($kv as $set) {
                list($k, $v) = explode(':', $set);

                switch($k) {
                    case 'province':
                        $model = $model->whereHas('clinic_addresses',  function($query) use ($v) {
                            $query->where('province', $v);
                        });
                    break;

                    case 'city':
                        $model = $model->whereHas('clinic_addresses',  function($query) use ($v) {
                            $query->where('city', $v);
                        });
                    break;
                    
                    case 'country':
                        $model = $model->whereHas('clinic_addresses',  function($query) use ($v) {
                            if (strtoupper($v) == 'OTHER') {
                                $query->where('country_code', '!=', 'PH');
                            } else {
                                $query->where('country_code', $v);
                            }
                        });
                    break;

                    case 'batch':
                        $model = $model->where('batch', $v);
                    break;

                    case 'specialization':
                        $model = $model->whereHas('profile',  function($query) use ($v) {
                            $query->where('specialty_id', $v);
                        });
                    break;
                }
            }

            // batch, specialization, city, province, country
        }   

        // dd($model->toSql(), $model->getBindings());
        
        return $model;
    }
}
