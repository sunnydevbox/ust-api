<?php

namespace Sunnydevbox\UST\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\FCM\FCMMessage;
use NotificationChannels\FCM\FCMMessageTopic;
use NotificationChannels\FCM\Test\FCMMessageTest;
use Sunnydevbox\UST\Models\Announcement;
use Sunnydevbox\UST\Models\Device;
use Sunnydevbox\UST\Models\Message;
use Sunnydevbox\UST\Models\User;

class MessageNotification extends Notification implements ShouldQueue
{
    use Queueable;

    protected $message;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['fcm', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return $this->message->toArray();
    }

    public function toFCM($notifiable)
    {
        return (new FCMMessage)
            ->notification([
                'title' => 'UST App - New Message Received',
                'body' => 'You have a new message.',
                'icon' => 'stock_ticker_update',
                'color' => '#f45342'
            ])->data($this->message->toArray());

    }
}
