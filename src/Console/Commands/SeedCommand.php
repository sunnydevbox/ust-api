<?php

namespace Sunnydevbox\UST\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Facades\Artisan;

class SeedCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ust:seed {--class=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run Seeder Files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->info('Running Database Seeder...');

        $class = $this->option('class');

        if ($class) {

            Artisan::call('db:seed',[
                '--class' => $class
            ]);

        } else {
            Artisan::call('db:seed',[
                // '--class' => \USTDatabaseSeeder::class
                '--class' => \USTUserSeeder::class
            ]);
        }

        $this->info(app('Illuminate\Contracts\Console\Kernel')->output());

        $this->info(' ... UST Database Seeder DONE');
    }

    public function fire()
    {
        echo 'fire';
    }
}
