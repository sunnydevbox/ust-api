<?php

namespace Sunnydevbox\UST\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Facades\Artisan;

class PublishConfigCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ust:publish-config';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish config files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Running CebuUnitedRebuilders migration files...');

        Artisan::call('vendor:publish');
        $this->info(app('Illuminate\Contracts\Console\Kernel')->output());
    }

    public function fire()
    {
        echo 'fire';
    }
}
