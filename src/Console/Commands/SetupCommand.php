<?php
namespace Sunnydevbox\UST\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Facades\Artisan;

class SetupCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ust:run-setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'UST - run setup';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    { 
        Artisan::call('ust:publish-config');
        Artisan::call('twuser:run-setup');
        Artisan::call('migrate');
        Artisan::call('ust:migrate', [
            '--action' => 'run',
        ]);
    }

    public function fire()
    {
        echo 'fire';
    }
}