<?php

namespace Sunnydevbox\UST\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Facades\Artisan;

class MigrateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ust:migrate {--action=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish migrations files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->info('Running UST migration files...');

        $action = $this->option('action');

        if ($action) {
            

            // $exitCode = Artisan::call('migrate' . $method, [
            //     '--path'    => 'vendor/sunnydevbox/tw-pim/src/Database/Migrations',
            // ]);

            // $exitCode = Artisan::call('migrate', [
            //     // '--path'    => 'vendor/sunnydevbox/tw-pim/src/Database/Migrations',
            //     '--action' => $method
            // ]);
                
            // $exitCode = $this->call('ust:migrate', [
            //     '--action' => $action
            // ]);


            $action = ($action == 'run') ? '' : ':' . $action;
            $exitCode = Artisan::call('migrate' . $action, [
                '--path'    => 'vendor/sunnydevbox/ust-api/database/migrations',
            ]);


            $this->info(app('Illuminate\Contracts\Console\Kernel')->output());
            
            $this->info(' ... UST migration DONE');
        } else {
            $this->error('Migration failed');
            $this->line('- You must specify the --action option [run/install/refresh/reset/rollback/status]');
        }
    }

    public function fire()
    {
        echo 'fire';
    }
}
