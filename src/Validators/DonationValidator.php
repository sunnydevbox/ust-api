<?php

namespace Sunnydevbox\UST\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class DonationValidator extends \Sunnydevbox\TWUser\Validators\UserValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'announcement_id' => 'required|exists:announcements,id',
            'user_id' => 'required|exists:users,id',
            'amount' => 'required|numeric'
        ],

    ];
}