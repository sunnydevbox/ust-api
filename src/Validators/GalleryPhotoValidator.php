<?php

namespace Sunnydevbox\UST\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class GalleryPhotoValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            // 'gallery_id' => 'required',
            'author_id' => 'required',
            'title' => '',
            'description' => '',
        ],
        ValidatorInterface::RULE_UPDATE => [
            // 'gallery_id' => 'required',
            'author_id' => 'required',
            'title' => '',
            'description' => '',
        ],

    ];
}