<?php

namespace Sunnydevbox\UST\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class AnnouncementValidator extends \Sunnydevbox\TWUser\Validators\UserValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'title' => 'required',
            'body' => 'required',
            'is_display_donate' => 'boolean|required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'title' => 'required',
            'body' => 'required',
            'is_display_donate' => 'boolean|required',
        ],
    ];
}