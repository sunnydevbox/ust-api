<?php

namespace Sunnydevbox\UST\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;

class MessageValidator extends \Sunnydevbox\TWUser\Validators\UserValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'receiver_id' => 'required|exists:users,id',
            'content' => 'required'
        ],
    ];
}