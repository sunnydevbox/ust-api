<?php

namespace Sunnydevbox\UST\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class PhotoValidator extends \Sunnydevbox\TWUser\Validators\UserValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'file' => 'image|required',
            'batch' => 'required|numeric|batch:1900',
            'categories' => 'array',
            'categories.*' => 'exists:categories,id'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'batch' => 'required|numeric|batch:1900',
            'categories' => 'array',
            'categories.*' => 'exists:categories,id'
        ],
    ];
}