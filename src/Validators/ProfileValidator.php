<?php

namespace Sunnydevbox\UST\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class ProfileValidator extends \Sunnydevbox\TWUser\Validators\UserValidator
{
    protected $attributes = [
        'society_affiliation' => 'society affiliation',
        'province'  => 'required',
        'country_code'  => 'country',
        'postal_code'  => 'postal code',
        'mobile'  => 'mobile number',
        'specialty_id'  => 'specialty',
        'license_no'  => 'license #',
        'affiliation_address'  => 'affiliation address',
    ];

    protected $messages = [
        'required' => 'The :attribute is required.',
    ];

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'society_affiliation' => 'required',
            'user_id'   => 'required',
            'city'      => 'required',
            'province'  => 'required',
            'country_code'  => 'required',
            'postal_code'  => 'required',
            'mobile'  => 'required',
            'specialty_id'  => 'required',
            'license_no'  => 'required',
            'affiliation_address'  => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'society_affiliation' => 'min:1',
            'user_id'   => 'min:1',
            'city'      => 'min:1',
            'province'  => 'min:1',
            'country_code'  => 'min:1',
            'postal_code'  => 'min:1',
            'mobile'  => 'min:1',
            'specialty_id'  => 'min:1',
            'license_no'  => 'min:1',
            'affiliation_address'  => 'min:1',

        ],
    ];
}