<?php

namespace Sunnydevbox\UST\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class ClinicAddressValidator extends \Sunnydevbox\TWUser\Validators\UserValidator
{
    protected $attributes = [
        'country_code' => "clinic's country",
        'name' => 'clinic name',
        'city' => "clinic's city",
        'province' => "clinic's province",
        'street' => "clinic's street",
    ];

    protected $messages = [
        'required' => 'The :attribute is required.',
    ];

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required',
            'country_code' => 'required',
            'city' => 'required',
            'province' => 'required',
            'street' => 'required',
            'user_id' => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'required',
            'country_code' => 'required',
            'city' => 'required',
            'province' => 'required',
            'street' => 'required',
            'user_id' => 'required',
        ],
    ];
}