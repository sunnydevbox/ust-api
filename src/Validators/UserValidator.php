<?php

namespace Sunnydevbox\UST\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class UserValidator extends \Sunnydevbox\TWUser\Validators\UserValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'email' => 'required|email',
            'password' => 'required|min:8|confirmed',
            'first_name' => 'required',
            'last_name' => 'required',
            'batch' => 'required|numeric|batch:1900',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'first_name' => 'required',
            'last_name' => 'required',
            'batch' => 'required|numeric|batch:1900',
            // 'profile_image' => 'string|required',
            'street' => 'required',
            'city' => 'required',
            'province' => 'required',
            'postal_code' => 'required',
            'country_code' => 'required',
            'mobile' => 'required',
            'specialty_id' => 'required_without:specialty_others|exists:specialties,id',
            'specialty_others' => 'required_without:specialty_id',
            // 'sub_specialty_id' => 'exists:specialties,id',
            // 'sub_specialty_others' => '',
            // 'sub_specialty_id_2' => 'exists:specialties,id',
            // 'sub_specialty_2_others' => '',
            'position' => 'required',
            'society_affiliation' => 'required',
            'affiliation_address' => 'required',
            'license_no' => 'required',
            'hospital_affiliation' => 'array|nullable',
            'hospital_affiliation.*.name' => 'required',
            'clinic_addresses' => 'array|nullable',
            'clinic_addresses.*.street' => 'required',
            'clinic_addresses.*.city' => '',
            'clinic_addresses.*.postal_code' => '',
            'clinic_addresses.*.country_code' => '',
            'is_info_shareable' => 'in:0,1',
            'is_email_clinical_shareable' => 'in:0,1'
        ],

        'UPDATE_PASSWORD' => [
            'password' => 'required',
            'new_password' => 'required|confirmed|min:6',
        ],
        
        'UPDATE_EMAIL' => [
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
        ],
    ];
}