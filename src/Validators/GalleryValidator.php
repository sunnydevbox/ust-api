<?php

namespace Sunnydevbox\UST\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class GalleryValidator extends \Sunnydevbox\TWUser\Validators\UserValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required|unique:galleries,name|min:4|max:100',
            'category' => '',
            'batch' => 'required|numeric',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'min:4|max:100',
            'category' => '',
            'batch' => 'numeric',
        ],

        'ATTACH_IMAGE' => [
            'gallery_id' => 'required',
            // 'image' => 'required|image',
            'title' => '',
            'description' => '',
        ],

        'DETACH_IMAGE' => [
            // 'gallery_id' => 'required',
            'photo_id' => 'required',
        ],
    ];
}