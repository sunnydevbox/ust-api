<?php

namespace Sunnydevbox\UST\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'type' => 'nullable|in:file,image',
             'file' => $this->get('type', 'file') .'|required',
             'target' => 'required|in:profile,announcements'
        ];

    }
}
