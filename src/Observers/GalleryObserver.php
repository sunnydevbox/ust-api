<?php
namespace Sunnydevbox\UST\Observers;

use Sunnydevbox\UST\Models\Gallery;

class GalleryObserver
{
    /**
     * Handle the User "created" event.
     *
     * @param  \App\User  $gallery
     * @return void
     */
    public function created(Gallery $gallery)
    {
    }

    /**
     * Handle the User "updated" event.
     *
     * @param  \App\User  $gallery
     * @return void
     */
    public function updated(Gallery $gallery)
    {
        //
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param  \App\User  $gallery
     * @return void
     */
    public function deleted(Gallery $gallery)
    {
        //
    }

    public function creating(Gallery $gallery)
    {
        /**
         * This is the default status upon creating the album
         */
        $gallery->status = $gallery::STATUS_PENDING;   
        
        if ($user = \Auth::user()) {
            $gallery->author_id = $user->id;
        }
    }
}