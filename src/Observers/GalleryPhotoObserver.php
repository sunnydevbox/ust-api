<?php
namespace Sunnydevbox\UST\Observers;

use Sunnydevbox\UST\Models\GalleryPhoto;

class GalleryPhotoObserver
{
    /**
     * Handle the User "created" event.
     *
     * @param  \Sunnydevbox\UST\Models\GalleryPhoto  $galleryPhoto
     * @return void
     */
    public function created(GalleryPhoto $galleryPhoto)
    {
    }

    /**
     * Handle the User "updated" event.
     *
     * @param  \Sunnydevbox\UST\Models\GalleryPhoto  $galleryPhoto
     * @return void
     */
    public function updated(GalleryPhoto $galleryPhoto)
    {
        //
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param  \Sunnydevbox\UST\Models\GalleryPhoto  $galleryPhoto
     * @return void
     */
    public function deleted(GalleryPhoto $galleryPhoto)
    {
        //
    }

    public function creating(GalleryPhoto $galleryPhoto)
    {
        /**
         * This is the default status upon creating the album
         */
        $galleryPhoto->status = $galleryPhoto::STATUS_PENDING;   
        
        if ($user = \Auth::user()) {
            $galleryPhoto->author_id = $user->id;
        }
    }
}