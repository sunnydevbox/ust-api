<?php
namespace Sunnydevbox\UST\Models;

use Illuminate\Support\Facades\Storage;
use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\UST\Traits\Pushable;

class Photo extends BaseModel
{
    use Pushable;

    protected $table = 'photos';

    protected $appends = ['file_path'];

    protected $fillable = [
        'file',
        'original_filename',
        'batch',
        'user_id'
    ];

    public function getFilePathAttribute()
    {
        return Storage::url($this->file);
    }

    public function subSpecialties()
    {
        return $this->hasMany(Specialty::class, 'parent_id', 'id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function isPhotoOwned(User $user)
    {
        return $this->user_id == $user->id;
    }
}