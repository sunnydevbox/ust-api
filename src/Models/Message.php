<?php
namespace Sunnydevbox\UST\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid;
use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\UST\Traits\Pushable;

class Message extends BaseModel
{
    use Pushable;

    protected $table = 'messages';

    protected $fillable = ['content','conversation_id'];

    public function getConversationId(User $sender, User $receiver)
    {
        return $this->select(['conversation_id'])->where('sender_id', $sender->id)
            ->where('receiver_id', $receiver->id)
            ->orWhere('sender_id', $receiver->id)
            ->where('receiver_id', $sender->id)->first()->conversation_id ?? null;
    }

    public function sender()
    {
        return $this->belongsTo(User::class);
    }

    public function receiver()
    {
        return $this->belongsTo(User::class);
    }
}