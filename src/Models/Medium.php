<?php
namespace Sunnydevbox\UST\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\UST\Traits\Pushable;

class Medium extends BaseModel
{
    use Pushable;

    protected $table = 'media';

    protected $fillable = ['image'];
    
    public function announcement()
    {
        return $this->belongsTo(Announcement::class);
    }
}