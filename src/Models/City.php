<?php
namespace Sunnydevbox\UST\Models;

use Illuminate\Support\Facades\Storage;
use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\UST\Traits\Pushable;
use Sunnydevbox\UST\Models\Photo;

class City extends BaseModel
{
    use Pushable;

    protected $table = 'cities';

    protected $fillables = ['name'];

    public function province()
    {
        return $this->belongsTo(Province::class);
    }
}