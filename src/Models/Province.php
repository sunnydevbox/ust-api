<?php
namespace Sunnydevbox\UST\Models;

use Illuminate\Support\Facades\Storage;
use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\UST\Traits\Pushable;
use Sunnydevbox\UST\Models\Photo;

class Province extends BaseModel
{
    use Pushable;

    protected $table = 'provinces';

    protected $fillables = ['name'];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function cities()
    {
        return $this->hasMany(City::class);
    }
}