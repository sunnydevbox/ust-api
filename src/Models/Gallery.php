<?php
namespace Sunnydevbox\UST\Models;

use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\UST\Observers\GalleryObserver;

class Gallery extends BaseModel
{
    const STATUS_PENDING='pending';
    const STATUS_ACTIVE='active';
    const STATUS_SUSPENDED='suspended';

    protected $table = 'galleries';

    protected $fillable = [
        'name',
        'description',
        'category',
        'batch',
        'status',
        'count',
        'author_id',

    ];

    public function photos()
    {
        return $this->hasMany(\Sunnydevbox\UST\Models\GalleryPhoto::class);
    }

    public function isActive()
    {
        return ($this->attributes['status'] == self::STATUS_ACTIVE)? true: false;
    }


    public function scopeSuspended($query)
    {
        $this->scopeStatus($query, self::STATUS_SUSPENDED);
    }

    public function scopePending($query)
    {
        $this->scopeStatus($query, self::STATUS_PENDING);
    }

    public function scopeActive($query)
    {
        $this->scopeStatus($query, self::STATUS_ACTIVE);
    }

    public function scopeStatus($query, $status)
    {
        $query->where('status', $status);
    }


    public static function boot()
    {
        parent::boot();
        Gallery::observe(GalleryObserver::class);
    }
}