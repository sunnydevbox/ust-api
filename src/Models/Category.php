<?php
namespace Sunnydevbox\UST\Models;

use Illuminate\Support\Facades\Storage;
use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\UST\Traits\Pushable;
use Sunnydevbox\UST\Models\Photo;

class Category extends BaseModel
{
    use Pushable;

    protected $table = 'categories';

    protected $fillables = ['slug', 'name'];
}