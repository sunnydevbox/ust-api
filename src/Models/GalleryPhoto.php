<?php
namespace Sunnydevbox\UST\Models;

use Bnb\Laravel\Attachments\HasAttachment;
use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\UST\Observers\GalleryPhotoObserver;

class GalleryPhoto extends BaseModel
{
    use HasAttachment;

    const STATUS_PENDING='pending';
    const STATUS_ACTIVE='active';
    const STATUS_SUSPENDED='suspended';

    protected $table = 'gallery_photos';

    protected $fillable = [
        'gallery_id',
        'author_id',
        'title',
        'filename',
        'description',
        'status',
        'category_id',
        'batch',
    ];


    public function attachmentMobile()
    {
        return $this->attachmentGroup('mobile');
    }

    public function isActive()
    {
        return ($this->attributes['status'] == self::STATUS_ACTIVE)? true: false;
    }


    public function category()
    {
        return $this->belongsTo(\Sunnydevbox\UST\Models\Category::class, 'category_id');
    }


    public function scopeSuspended($query)
    {
        $this->scopeStatus($query, self::STATUS_SUSPENDED);
    }

    public function scopePending($query)
    {
        $this->scopeStatus($query, self::STATUS_PENDING);
    }

    public function scopeActive($query)
    {
        $this->scopeStatus($query, self::STATUS_ACTIVE);
    }

    public function scopeStatus($query, $status)
    {
        $query->where('status', $status);
    }


    public static function boot()
    {
        parent::boot();
        GalleryPhoto::observe(GalleryPhotoObserver::class);
    }
}