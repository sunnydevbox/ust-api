<?php
namespace Sunnydevbox\UST\Models;

use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\UST\Traits\Pushable;

class Profile extends BaseModel
{
    use Pushable;

    protected $table = 'profiles';

    protected $fillable = [
        'profile_image',
        'street_no',
        'street',
        'city',
        'province',
        'country_code',
        'postal_code',
        'mobile',
        'specialty_id',
        'specialty_others',
        'sub_specialty_id',
        'sub_specialty_others',
        'sub_specialty_id_2',
        'sub_specialty_2_others',
        'position',
        'society_affiliation',
        'affiliation_address',
        'license_no',
        'is_info_shareable',
        'is_email_clinical_shareable',
        'user_id',
    ];

    public $appends = [
        'profile_image_mobile',
    ];

    public function specialty()
    {
        return $this->belongsTo(Specialty::class);
    }

    public function sub_specialty()
    {
        return $this->belongsTo(Specialty::class);
    }

    public function sub_specialty2()
    {
        return $this->belongsTo(Specialty::class, 'sub_specialty_id_2');
    }
    
    // public function getProfileImageAttribute()
    // {
    //     if ($this->attributes['profile_image'] && strlen($this->attributes['profile_image'])) {
    //         return \Storage::url($this->attributes['profile_image']);
    //     }

    //     return $this->attributes['profile_image'];
    // }

    public function getProfileImageMobileAttribute()
    {   
        if (isset($this->attributes['profile_image']) && $this->attributes['profile_image']) {
            $image = str_replace('profile/', '', $this->attributes['profile_image']);

            if (preg_match('/^http/', $this->attributes['profile_image'])) {
                return $this->attributes['profile_image'] . '/mobile';
            } else { 
                return url('profile/' . $image . '/mobile');
            }
        }

        return null;
    }

    public function setIsEmailClinicalShareableAttribute($value)
    {
        $this->attributes['is_email_clinical_shareable'] = ($value) ? 1 : 0;
    }

    public function setIsInfoShareableAttribute($value)
    {
        $this->attributes['is_info_shareable'] = ($value) ? 1 : 0;
    }

    public function scopeGetBy($query, $column, $value, $operator = '=')
    {
        $query->where($column, $operator, $value);
    }

}