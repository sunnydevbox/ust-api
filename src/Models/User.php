<?php
namespace Sunnydevbox\UST\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\CausesActivity;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Collection;
use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\UST\Traits\Pushable;
use Sunnydevbox\UST\Models\Profile;

class User extends \Sunnydevbox\TWUser\Models\User
{
    //use \Sunnydevbox\TWCore\Repositories\TWMetaTrait;
    use Notifiable;
    use SoftDeletes;
    use HasRoles;
    use CausesActivity;
    use Pushable;


    CONST STATUS_ACTIVE = 'active';
    CONST STATUS_INACTIVE = 'inactive';

    protected $table = 'users';

    protected $fillable = ['email', 'first_name', 'middle_name', 'last_name', 'batch', 'password', 'verification_token','is_verified'];

    protected $dates = ['is_verified'];

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function hospital_affiliations()
    {
        return $this->hasMany(HospitalAffiliation::class);
    }

    public function clinic_addresses()
    {
        return $this->hasMany(ClinicAddress::class);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function devices()
    {
        return $this->hasMany(Device::class);
    }

    public function routeNotificationForFCM()
    {
        return $this->devices->pluck('advertising_id')->toArray();
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    public function scopeInactive($query)
    {
        return $query->where('status', self::STATUS_INACTIVE);
    }
}