<?php
namespace Sunnydevbox\UST\Models;

class Attachment extends Bnb\Laravel\Attachments\Attachment
{
    protected $table = 'categories';

    protected $fillables = ['slug', 'name'];
}