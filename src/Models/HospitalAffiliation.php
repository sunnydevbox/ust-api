<?php
namespace Sunnydevbox\UST\Models;

use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\UST\Traits\Pushable;

class HospitalAffiliation extends BaseModel
{
    use Pushable;

    protected $table = 'hospital_affiliations';

    protected $fillable = [
        'name',
        'user_id',
    ];
}