<?php
namespace Sunnydevbox\UST\Models;

use Illuminate\Support\Facades\Storage;
use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\UST\Traits\Pushable;
use Sunnydevbox\UST\Models\Photo;

class Country extends BaseModel
{
    use Pushable;

    protected $table = 'countries';

    protected $fillables = ['country_code', 'name'];

    public function provinces()
    {
        return $this->hasMany(Province::class);
    }
}