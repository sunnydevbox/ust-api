<?php
namespace Sunnydevbox\UST\Models;

use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\UST\Traits\Pushable;

class Specialty extends BaseModel
{
    use Pushable;

    protected $table = 'specialties';

    protected $fillable = [
        'name',
        'parent_id',
    ];

    public function subSpecialties()
    {
        return $this->hasMany(Specialty::class, 'parent_id', 'id');
    }
}