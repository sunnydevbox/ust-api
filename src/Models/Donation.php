<?php
namespace Sunnydevbox\UST\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\UST\Traits\Pushable;

class Donation extends BaseModel
{
    use Pushable;

    protected $table = 'donations';

    protected $fillable = ['amount','user_id', 'announcement_id'];

    public function announcement()
    {
        return $this->belongsTo(Announcement::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}