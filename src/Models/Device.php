<?php
namespace Sunnydevbox\UST\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\UST\Traits\Pushable;

class Device extends BaseModel
{
    use Pushable;

    protected $table = 'devices';

    protected $fillable = [
        'advertising_id',
        'user_id',
        'type',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}