<?php
namespace Sunnydevbox\UST\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\UST\Traits\Pushable;
use Bnb\Laravel\Attachments\HasAttachment;

class Announcement extends BaseModel
{
    use Pushable;
    use SoftDeletes;
    use HasAttachment;

    protected $table = 'announcements';

    protected $fillable = ['title', 'body', 'is_display_donate', 'highlighted_at'];

    protected $dates = ['highlighted_at'];

    protected $with = ['media'];

    public function users()
    {
        return $this->belongsToMany(\Sunnydevbox\UST\Models\User::class)->withPivot('viewed_at');
    }

    public function media()
    {
        return $this->hasMany(Medium::class);
    }

}