<?php
namespace Sunnydevbox\UST\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\UST\Traits\Pushable;

class Alumnus extends BaseModel
{
    use Pushable;

    protected $table = 'alumni';

    protected $fillable = ['first_name', 'last_name', 'batch', 'middle_name', 'suffix'];
}