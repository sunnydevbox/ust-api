<?php
namespace Sunnydevbox\UST\Models;

use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\UST\Traits\Pushable;

class ClinicAddress extends BaseModel
{
    use Pushable;

    protected $table = 'clinic_addresses';

    protected $fillable = [
        'name',
        'street_no',
        'street',
        'city',
        'province',
        'postal_code',
        'country_code',
        'user_id',
    ];
}