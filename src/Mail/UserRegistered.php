<?php

namespace Sunnydevbox\UST\Mail;
//namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sunnydevbox\UST\Models\User;

class UserRegistered extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('ust::mail.verification')
                    ->with([
                        'user' => $this->user->first_name,
                        'verification_url' => 

                          config('app.url')  // CALL THIS FROM the config() instead
                        . '/users/'
                        . urlencode($this->user->email) 
                        . '/verify/'
                        . $this->user->verification_token,
                    ])
                    ->subject('UST Alumni :: Please verify your email')
                    //->from()
                    ->to($this->user->email, $this->user->first_name);
    }
}
