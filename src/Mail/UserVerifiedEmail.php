<?php

namespace Sunnydevbox\UST\Mail;
//namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sunnydevbox\UST\Models\User;

class UserVerifiedEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->user->first_name.' '.$this->user->last_name;

        return $this->view('ust::mail.verified')
                    ->with([
                        'name' => $name,
                    ])
                    ->subject('UST Alumni :: Welcome to UST!')
                    //->from()
                    ->to($this->user->email, $this->user->first_name);
    }
}
