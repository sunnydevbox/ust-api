<?php

namespace Sunnydevbox\UST\Traits;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait Pushable
{
    public function push($parent = null, $key = null)
    {
        if ($parent) {
            if (! $parent->$key()->save($this)) {
                return false;
            }
        }
        if (! $this->save()) {
            return false;
        }

        // To sync all of the relationships to the database, we will simply spin through
        // the relationships and save each model via this "push" method, which allows
        // us to recurse into all of these nested relations for the model instance.
        foreach ($this->relations as $key => $models) {
            $models = $models instanceof Collection ? $models->all() : [$models];

            foreach (array_filter($models) as $model) {
                if ($this->$key() instanceof HasMany || $this->$key() instanceof HasOne) {
                    if (! $model->push($this, $key)) {
                        return false;
                    }
                } else {
                    if (! $model->push()) {
                        return false;
                    }
                }
            }
        }

        return true;
    }
}