<?php

namespace Sunnydevbox\UST;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        // User signed up for an account
        'Sunnydevbox\UST\Events\UserRegisteredEvent' => [
            'Sunnydevbox\UST\Listeners\SendActivationCodeEventListener',
        ],

        'Sunnydevbox\UST\Events\ResendUserActivationCode' => [
            'Sunnydevbox\UST\Listeners\SendActivationCodeEventListener',
        ],


        // USER Verified his email address
        'Sunnydevbox\UST\Events\UserVerifiedEmailEvent' => [
            'Sunnydevbox\UST\Listeners\VerifiedAccount',
            'Sunnydevbox\UST\Listeners\SendEmailVerificationConfirmation',
        ],
        // User requested to reset his password
        'Sunnydevbox\UST\Events\UserRequestResetPasswordEvent' => [
            'Sunnydevbox\UST\Listeners\SendResetPasswordTokenEventListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
