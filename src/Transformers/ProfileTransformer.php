<?php

namespace Sunnydevbox\UST\Transformers;

use Sunnydevbox\TWUser\Transformers\RoleTransformer;
use League\Fractal\TransformerAbstract;
use Storage;
use Sunnydevbox\UST\Models\Profile;

class ProfileTransformer extends TransformerAbstract
{
    public function transform(Profile $profile)
    {
        $profileImage = $profile->profile_image;
        $profileImageMobile = $profile->profile_image_mobile;

        if ($profileImage) {
            if (strpos($profileImage, 'http') !== 0) {
                $profileImage = Storage::url($profile->profile_image);
            }

            if (strpos($profile->profile_image_mobile, 'http') !== 0) {
                $profileImageMobile = Storage::url($profile->profile_image_mobile);
            }
        }
        
        return [
            'id' => $profile->id,
            'profile_image' => $profileImage, // ($profile->profile_image) ? ['path' => $profile->profile_image, 'url' => Storage::url($profile->profile_image)] : null,
            'profile_image_mobile' => $profileImageMobile, //($profile->profile_image) ? ['url' => Storage::url($profile->profile_image_mobile)] : null,
            'street_no' => $profile->street_no,
            'street' => $profile->street,
            'city' => $profile->city,
            'province' => $profile->province,
            'postal_code' => $profile->postal_code,
            'country_code' => $profile->country_code,
            'mobile' => $profile->mobile,
            'specialty_id' => $profile->specialty_id,
            'specialty_name' => $profile->specialty ? $profile->specialty->name : null,
            'specialty_others' => $profile->specialty_others,
            'sub_specialty_id' => $profile->sub_specialty_id,
            'sub_specialty_name' => $profile->sub_specialty  ? $profile->sub_specialty->name : null,
            'sub_specialty_others' => $profile->sub_specialty_others,
            'sub_specialty_id_2' => $profile->sub_specialty_id_2,
            'sub_specialty_2_name' =>  $profile->sub_specialty2  ? $profile->sub_specialty2->name : null,
            'sub_specialty_2_others' => $profile->sub_specialty_2_others,
            'position' => $profile->position,
            'society_affiliation' => (int) $profile->society_affiliation,
            'affiliation_address' => $profile->affiliation_address,
            'license_no' => $profile->license_no,
            'is_info_shareable' => $profile->is_info_shareable,
            'is_email_clinical_shareable' => $profile->is_email_clinical_shareable
        ];
    }
}