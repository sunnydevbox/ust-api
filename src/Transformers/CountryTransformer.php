<?php

namespace Sunnydevbox\UST\Transformers;

use Sunnydevbox\UST\Models\Country;
use League\Fractal\TransformerAbstract;

class CountryTransformer extends TransformerAbstract
{

    protected $availableIncludes = [];

    public function transform(Country $country)
    {
        return $country->toArray();
    }
}