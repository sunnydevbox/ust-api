<?php

namespace Sunnydevbox\UST\Transformers;

use Sunnydevbox\TWUser\Transformers\RoleTransformer;
use League\Fractal\TransformerAbstract;
use Storage;
use Sunnydevbox\UST\Models\HospitalAffiliation;

class HospitalAffiliationsTransformer extends TransformerAbstract
{
    public function transform(HospitalAffiliation $hospitalAffiliation)
    {
        return [
            'id' => $hospitalAffiliation->id,
            'name' => $hospitalAffiliation->name,
        ];
    }
}