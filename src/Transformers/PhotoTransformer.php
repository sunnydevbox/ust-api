<?php

namespace Sunnydevbox\UST\Transformers;

use Sunnydevbox\TWUser\Transformers\RoleTransformer;
use Sunnydevbox\UST\Models\Announcement;
use League\Fractal\TransformerAbstract;
use Storage;
use Auth;
use Sunnydevbox\UST\Models\Photo;

class PhotoTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['categories'];

    public function transform(Photo $photo)
    {
        $data = $photo->toArray();

        return $data;
    }

    public function includeCategories(Photo $photo)
    {
        return $this->collection($photo->categories, new CategoryTransformer);
    }
}