<?php

namespace Sunnydevbox\UST\Transformers;

use Sunnydevbox\UST\Models\City;
use Sunnydevbox\UST\Models\Country;
use League\Fractal\TransformerAbstract;

class CityTransformer extends TransformerAbstract
{

    protected $availableIncludes = [];

    public function transform(City $city)
    {
        return $city->toArray();
    }
}