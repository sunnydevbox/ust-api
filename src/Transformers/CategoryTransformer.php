<?php

namespace Sunnydevbox\UST\Transformers;

use Sunnydevbox\TWUser\Transformers\RoleTransformer;
use Sunnydevbox\UST\Models\Announcement;
use League\Fractal\TransformerAbstract;
use Storage;
use Auth;
use Sunnydevbox\UST\Models\Category;
use Sunnydevbox\UST\Models\Photo;

class CategoryTransformer extends TransformerAbstract
{

    public function transform(Category $category)
    {
        $data = $category->toArray();

        return $data;
    }

}