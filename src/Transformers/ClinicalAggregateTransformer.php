<?php

namespace Sunnydevbox\UST\Transformers;

use League\Fractal\TransformerAbstract;
use Sunnydevbox\UST\Models\ClinicAddress;

class ClinicalAggregateTransformer extends TransformerAbstract
{
    public function transform(ClinicAddress $clinicAddress)
    {
        return $clinicAddress->toArray();
        return [
            'id' => $clinicAddress->id,
            'name' => $clinicAddress->name,
            'street_no' => $clinicAddress->street_no,
            'street'  => $clinicAddress->street,
            'city' => $clinicAddress->city,
            'province' => $clinicAddress->province,
            'postal_code' => $clinicAddress->postal_code,
            'country_code'=> $clinicAddress->country_code,
        ];
    }
}