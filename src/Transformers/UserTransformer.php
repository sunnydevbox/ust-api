<?php

namespace Sunnydevbox\UST\Transformers;

use Sunnydevbox\TWUser\Transformers\RoleTransformer;
use Sunnydevbox\UST\Models\User;
use League\Fractal\TransformerAbstract;
use Storage;

class UserTransformer extends TransformerAbstract
{
    /**
     * Include user profile data by default
     */
    protected $defaultIncludes =   [];
    protected $availableIncludes = [
        'roles',
        'profile',
        'hospital_affiliations',
        'clinic_addresses',
    ];

    protected $mode = 'complete'; // 'basic', 'complete'


    public function transform(User $user)
    {
        
        if (request()->get('filter')) {
            

            $relations = $user->getRelations();

            $r = collect($user->toArray())->only(explode(';', request()->get('filter')));
            // var_dump($r);
            // var_dump($relations);
            $merged = $r->merge($relations)->all();

            return ($merged);
            // return ($r->all());
        } else {
            return $user->toArray();
        }
        // return [
        //     'id' => $user->id,
        //     'email' => $user->email,
        //     'first_name' => $user->first_name,
        //     'middle_name' => $user->middle_name,
        //     'last_name' => $user->last_name,
        //     'batch' => $user->batch,
        //     'is_verified' => (bool) $user->verified_at,
        //     'token' => $user->token,
        // ];
    }

    public function includeRoles(User $user)
    {
        if ($user->roles) {
            return $this->collection($user->roles, new RoleTransformer);
        }
    }

    public function includeProfile(User $user)
    {
        if ($profile = $user->profile) {
            return $this->item($user->profile, new ProfileTransformer);
        }
    }

    public function includeHospitalAffiliations(User $user)
    {
        if($affiliations = $user->hospital_affiliations){
            return $this->collection($affiliations, new HospitalAffiliationsTransformer);
        }
    }

    public function includeClinicAddresses(User $user)
    {
        if($clinical_addresses = $user->clinic_addresses){
            return $this->collection($clinical_addresses, new ClinicalAddressesesTransformer);
        }
    }
}