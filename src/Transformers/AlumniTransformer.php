<?php

namespace Sunnydevbox\UST\Transformers;

use Sunnydevbox\TWUser\Transformers\RoleTransformer;
use Sunnydevbox\UST\Models\User;
use League\Fractal\TransformerAbstract;
use Storage;

class AlumniTransformer extends TransformerAbstract
{
    /**
     * Include user profile data by default
     */
    protected $defaultIncludes =   [
        'profile',
        'hospital_affiliations',
        'clinic_addresses',
    ];
    protected $availableIncludes = [];

    protected $mode = 'complete'; // 'basic', 'complete'


    public function transform(User $user)
    {

        return $user->toArray();
    }

    public function includeRoles(User $user)
    {
        if ($user->roles) {
            return $this->collection($user->roles, new RoleTransformer);
        }
    }

    public function includeProfile(User $user)
    {

        if ($user->profile && $user->profile->is_info_shareable == 1) {
            $profile = $user->profile;
            return $this->item($profile, new ProfileTransformer);
        }
    }

    public function includeHospitalAffiliations(User $user)
    {
        if($affiliations = $user->hospital_affiliations){
            return $this->collection($affiliations, new HospitalAffiliationsTransformer);
        }
    }

    public function includeClinicAddresses(User $user)
    {

        if($user->clinic_addresses && $user->profile && $user->profile->is_email_clinical_shareable == 1){
            $clinical_addresses = $user->clinic_addresses;
            return $this->collection($clinical_addresses, new ClinicalAddressesesTransformer);
        }

        return null;
    }
}