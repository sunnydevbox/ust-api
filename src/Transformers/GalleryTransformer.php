<?php

namespace Sunnydevbox\UST\Transformers;

use Sunnydevbox\UST\Models\Gallery;
use League\Fractal\TransformerAbstract;

class GalleryTransformer extends TransformerAbstract
{
    public function transform(Gallery $gallery)
    {
        return $gallery->toArray();
    }
}