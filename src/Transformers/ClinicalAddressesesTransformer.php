<?php

namespace Sunnydevbox\UST\Transformers;

use Sunnydevbox\TWUser\Transformers\RoleTransformer;
use League\Fractal\TransformerAbstract;
use Storage;
use Sunnydevbox\UST\Models\ClinicAddress;
use Sunnydevbox\UST\Models\HospitalAffiliation;

class ClinicalAddressesesTransformer extends TransformerAbstract
{
    public function transform(ClinicAddress $clinicAddress)
    {
        return [
            'id' => $clinicAddress->id,
            'name' => $clinicAddress->name,
            'street_no' => $clinicAddress->street_no,
            'street'  => $clinicAddress->street,
            'city' => $clinicAddress->city,
            'province' => $clinicAddress->province,
            'postal_code' => $clinicAddress->postal_code,
            'country_code'=> $clinicAddress->country_code,
        ];
    }
}