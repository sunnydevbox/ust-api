<?php

namespace Sunnydevbox\UST\Transformers;

use Sunnydevbox\TWUser\Transformers\RoleTransformer;
use Sunnydevbox\UST\Models\Announcement;
use League\Fractal\TransformerAbstract;
use Storage;
use Auth;

class AnnouncementTransformer extends TransformerAbstract
{
    public function transform(Announcement $announcement)
    {
        
        $data = $announcement->toArray();

        $data['media'] = $announcement->attachments->map(function($attachment) {
            return collect($attachment->toArray())->only(['id', 'filepath', 'filename', 'url','filetype', 'uuid', 'filesize', 'group']);
        });

        $data['is_read'] = ($announcement->users()->where('user_id', Auth::user()->id)->whereNotNull('viewed_at')->count() > 0) ? true : false;

        return $data;
    }
}