<?php

namespace Sunnydevbox\UST\Transformers;

use League\Fractal\TransformerAbstract;
use Sunnydevbox\UST\Models\Donation;

class DonationsTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['user', 'announcement'];

    public function transform(Donation $donation)
    {
        $data = $donation->toArray();

        return $data;
    }

    public function includeUser(Donation $donation)
    {
        return $this->item($donation->user, new UserTransformer);
    }

    public function includeAnnouncement(Donation $donation)
    {
        return $this->item($donation->announcement, new AnnouncementTransformer);
    }
}