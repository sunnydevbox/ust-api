<?php

namespace Sunnydevbox\UST\Transformers;

use Sunnydevbox\UST\Models\Country;
use League\Fractal\TransformerAbstract;
use Sunnydevbox\UST\Models\Province;

class ProvinceTransformer extends TransformerAbstract
{

    protected $availableIncludes = [];

    public function transform(Province $province)
    {
        return $province->toArray();
    }
}