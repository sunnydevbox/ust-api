<?php

namespace Sunnydevbox\UST\Transformers;

use Sunnydevbox\UST\Models\GalleryPhoto;
use League\Fractal\TransformerAbstract;

class GalleryPhotoTransformer extends TransformerAbstract
{
    public function transform(GalleryPhoto $photo)
    {
        return $photo->toArray();
    }
}