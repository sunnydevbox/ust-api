<?php

namespace Sunnydevbox\UST\Transformers;

use League\Fractal\TransformerAbstract;
use Sunnydevbox\UST\Models\Specialty;

class SpecialtiesTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'subSpecialties',
    ];

    public function transform(Specialty $specialty)
    {
        return [
            'id' => $specialty->id,
            'name'=> $specialty->name,

        ];
    }

    public function includeSubSpecialties(Specialty $specialty)
    {
        if($sub_specialties = $specialty->subSpecialties){
            return $this->collection($sub_specialties, new SpecialtiesTransformer);
        }
    }
}