<?php

namespace Sunnydevbox\UST\Transformers;

use League\Fractal\TransformerAbstract;
use Sunnydevbox\UST\Models\Message;

class MessageTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['sender','receiver'];

    public function transform(Message $message)
    {
        return $message->toArray();
    }

    public function includeSender(Message $message)
    {
        return $this->item($message->sender, new UserTransformer);
    }

    public function includeReceiver(Message $message)
    {
        return $this->item($message->receiver, new UserTransformer);
    }
}