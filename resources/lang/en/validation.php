<?php

return [
    'month_days_format' => 'Hyeya',
    'messages' => [
        'month_days' => 'The :attribute field is required.',
    ],
    'attributes' => [
        'month_days' => 'month-days',
    ],
];