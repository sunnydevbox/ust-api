<p>Hi {{ $user->first_name }},</p>

<p>You have requested to reset your password.</p>

<p>Follow this <a href="{{ $reset_url }}">link</a> to continue </p>


<p>or</p>

<p> you can just disregard this email.</p>

<p>If you think your account is in danger of breach, please update your password immediately.</p>