<p>Hi {{ $user->first_name }},</p>

<p>Click on this link, <a href="{{ $verification_url }}">{{ $verification_url }}</a> , or copy and paste it into your browser's address bar to complete the process</p>
