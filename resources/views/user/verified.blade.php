<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Bare - Start Bootstrap Template</title>

  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>

<body>
  <!-- Page Content -->
  <div class="container">
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <div class="jumbotron col-sm-8 offset-md-2">


        @if ($error)
          <p>There appears to be a problem verifying your account: <strong>{{ $error }}</strong></p>
          <p>Please do not hesitate to contact our <a href="mailto:app_admin@ustmaa.org">administrator</a> for further assistance.</p>

          <p>Contact the administrator at <a href="mailto:app_admin@ustmaa.org">app_admin@ustmaa.org</a>.</p>
          <hr class="my-4">
          <p>Cheers,<br/>
                  <strong>UST Team</strong>
          </p>

          
        @else
          <h1 class="display-4">Hi {{ $user->first_name }},</h1>
          <p class="lead">You have verified your email. Thank you.</p>
          <p class="lead">Welcome to UST!</p>
          <hr class="my-4">
          <p>Cheers,<br/>
                  <strong>UST Team</strong>
          </p>
        @endif 
    </div>

 </div>

</body>

</html>

