<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Employee Model
    |--------------------------------------------------------------------------
    |
    | Developer can override this with his class that extends 
    | the main Employee Model
    |
    */
    'model_user'             => \Sunnydevbox\UST\Models\User::class,

];